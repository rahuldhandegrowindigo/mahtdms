package com.newtrail.mahyco.trail;

import android.database.Cursor;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class forget_password extends AppCompatActivity {
    databaseHelper databaseHelper1;
    Config config;
    public String userCode;
    private TextView txtView,lbltype,txtRegister,txtUpdate,txtForget;
    public EditText txtentermobile,txtEnterotp;
    private CardView btnLogin;
    public Messageclass msclass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        getSupportActionBar().setTitle("Update Password");
        databaseHelper1=new databaseHelper(this);
        msclass=new Messageclass(this);
        txtentermobile=(EditText) findViewById(R.id.txtentermobile);
        txtEnterotp=(EditText) findViewById(R.id.txtEnterotp);
        btnLogin= (CardView)findViewById(R.id.btnLogin);
        Cursor data = databaseHelper1.fetchusercode();

        if (data.getCount()==0)
        {

        }else {
            data.moveToFirst();
            if(data!=null)
            {
                do
                {
                    userCode=data.getString((data.getColumnIndex("user_code")));
                }while(data.moveToNext());

            }data.close();
            txtentermobile.setText(userCode);

        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String InsertQuery = "update UserMaster set User_pwd = '"+txtEnterotp.getText().toString()+"' where user_code = '"+txtentermobile.getText().toString()+"' ";
                databaseHelper1.runQuery(InsertQuery);
                msclass.showMessage("Password Update successfully");
                return;
            }
        });
    }
}
