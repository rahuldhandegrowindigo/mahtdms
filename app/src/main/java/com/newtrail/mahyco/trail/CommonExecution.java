package com.newtrail.mahyco.trail;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class CommonExecution {


    Context context;
    String returnstring;
    ProgressDialog dialog;
    public String Bredderurlpath;
    public String MDOurlpath;

    public CommonExecution(Context context) {
        this.context = context;
        dialog = new ProgressDialog(this.context);
        //dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        // Urlpath="http://farm.mahyco.com/TestHandler.ashx";
        Bredderurlpath = "http://cmr.mahyco.com/BreaderDataHandler.ashx";
        MDOurlpath = "http://cmr.mahyco.com/MDOHandler.ashx";
    }

    public static void setBlinkingTextview(ImageView tv, long milliseconds, long offset) {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(milliseconds); //You can manage the blinking time with this parameter
        anim.setStartOffset(offset);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        tv.startAnimation(anim);
    }

    public class BreederMasterData extends AsyncTask<String, String, String> {

        private int action;
        private String username;
        private String password;
        private String imei;


        public BreederMasterData(int action, String username, String password, String imei) {
            this.action = action;
            this.username = username;
            this.password = password;
            this.imei = imei;
        }

        protected void onPreExecute() {
            dialog.setMessage("Loading....");
            dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            HttpClient httpclient = new DefaultHttpClient();
            //HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), 10000); // Timeout Limit

            StringBuilder builder = new StringBuilder();
            List<NameValuePair> postParameters = new ArrayList<NameValuePair>(2);
            postParameters.add(new BasicNameValuePair("from", "insertbreederData"));
            // postParameters.add(new BasicNameValuePair("xmlString",""));
            String Urlpath1 = Bredderurlpath + "?username=" + username + "&action=" + action + "&password=" + password + "&imei=" + imei + "";
            HttpPost httppost = new HttpPost(Urlpath1);

            // StringEntity entity;
            // entity = new StringEntity(request, HTTP.UTF_8);

            httppost.addHeader("Content-type", "application/x-www-form-urlencoded");
            // httppost.setHeader("Content-Type","text/xml;charset=UTF-8");
            try {
                httppost.setEntity(new UrlEncodedFormEntity(postParameters));
                UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
                httppost.setEntity(formEntity);

                HttpResponse response = httpclient.execute(httppost);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }
                    returnstring = builder.toString();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                returnstring = e.getMessage().toString();
                //dialog.dismiss();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                returnstring = e.getMessage().toString();
                // dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
                returnstring = e.getMessage().toString();
                //  dialog.dismiss();
            }

            // dialog.dismiss();
            return builder.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    public class BreederMasterGoogleData extends AsyncTask<String, String, String> {


        private String email;

        public BreederMasterGoogleData(String email) {
            this.email = email;

        }

        protected void onPreExecute() {
            dialog.setMessage("Loading....");
            dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            HttpClient httpclient = new DefaultHttpClient();

            StringBuilder builder = new StringBuilder();
            List<NameValuePair> postParameters = new ArrayList<NameValuePair>(2);


            postParameters.add(new BasicNameValuePair("from", "breederVerifyUserEmail"));

            String Urlpath1 = Bredderurlpath + "/" + "breederVerifyUserEmail" + "?userMail=" + email + "";
            HttpPost httppost = new HttpPost(Urlpath1);
            httppost.addHeader("Content-type", "application/x-www-form-urlencoded");
            try {
                httppost.setEntity(new UrlEncodedFormEntity(postParameters));
                UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
                httppost.setEntity(formEntity);

                HttpResponse response = httpclient.execute(httppost);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }
                    returnstring = builder.toString();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                returnstring = e.getMessage().toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                returnstring = e.getMessage().toString();
            } catch (Exception e) {
                e.printStackTrace();
                returnstring = e.getMessage().toString();
            }

            return builder.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    public void bindimage(ImageView img, String Path) {
        try {
            File imgFile = new File(Path);

            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                img.setImageBitmap(myBitmap);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    //// TO download Data from year , session, staffcode

    public class BreederDataDownload extends AsyncTask<String, String, String> {

        private int action;
        private String ddlyear;
        private String ddlsession;
        private String usercode;
        private String TFA;


        public BreederDataDownload(int action, String ddlyear, String ddlsession, String usercode, String TFA) {
            this.action = action;
            this.ddlyear = ddlyear;
            this.ddlsession = ddlsession;
            this.usercode = usercode;
            this.TFA = TFA;
        }

        protected void onPreExecute() {
            dialog.setMessage("Loading....");
            dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            HttpClient httpclient = new DefaultHttpClient();
            //HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), 10000); // Timeout Limit

            StringBuilder builder = new StringBuilder();
            List<NameValuePair> postParameters = new ArrayList<NameValuePair>(2);
            postParameters.add(new BasicNameValuePair("from", "insertDownloadData"));
            // postParameters.add(new BasicNameValuePair("xmlString",""));
            String Urlpath1 = Bredderurlpath + "?action=" + action + "&ddlyear=" + ddlyear + "&ddlsession=" + ddlsession + "&usercode=" + usercode + "&TFA=" + TFA + "";
            HttpPost httppost = new HttpPost(Urlpath1);

            // StringEntity entity;
            // entity = new StringEntity(request, HTTP.UTF_8);

            httppost.addHeader("Content-type", "application/x-www-form-urlencoded");
            // httppost.setHeader("Content-Type","text/xml;charset=UTF-8");
            try {
                httppost.setEntity(new UrlEncodedFormEntity(postParameters));
                UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
                httppost.setEntity(formEntity);

                HttpResponse response = httpclient.execute(httppost);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }
                    returnstring = builder.toString();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                returnstring = e.getMessage().toString();
                // dialog.dismiss();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                returnstring = e.getMessage().toString();
                //  dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
                returnstring = e.getMessage().toString();
                //  dialog.dismiss();
            }

            // dialog.dismiss();
            return builder.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }


}
