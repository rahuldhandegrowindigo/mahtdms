package com.newtrail.mahyco.trail;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class trial_ListAdaptor extends RecyclerView.Adapter<trial_ListAdaptor.DataObjectHolder> {
    private List<TrailReportModel> objects = new ArrayList<TrailReportModel>();

    private Context context;
    private LayoutInflater layoutInflater;
    int imageResourceId = 0;
    boolean isFromObservation;
    boolean isFromSurvey;

    public trial_ListAdaptor(List<TrailReportModel> getlist, Context context, boolean isFromObservation, boolean isFromSurvey) {
        this.context = context;
        this.objects = getlist;
        this.isFromObservation = isFromObservation;
        this.isFromSurvey = isFromSurvey;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public trial_ListAdaptor.DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trail_details, parent, false);

        return new trial_ListAdaptor.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull trial_ListAdaptor.DataObjectHolder holder, final int position) {

        holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.Whitecl));

        holder.txt_LocationDetails.setText(objects.get(position).getLocation());
        holder.txt_TrialCodedetails.setText(objects.get(position).getTrailcode());
        holder.txt_TrialTagDetails.setText(objects.get(position).getTagged());
        holder.txt_TrialSegmentDetails.setText(objects.get(position).getTxt_TrialSegmentDetails());

        if (holder.txt_TrialTagDetails.getText().equals("T")) {
            holder.btnPLD.setVisibility(View.GONE);
        }
        holder.rel_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( isFromObservation){
                    Intent i = new Intent(context, Testimonial.class);
                    i.putExtra("trailCode", objects.get(position).getTrailcode());
                    context.startActivity(i);
                }else if(isFromSurvey){

                    Intent i = new Intent(context, survey.class);
                    i.putExtra("trailCode", objects.get(position).getTrailcode());
                    context.startActivity(i);

                }else {
                    Intent i = new Intent(context, FeedbackDetailsActivity.class);
                    i.putExtra("trailCode", objects.get(position).getTrailcode());
                    context.startActivity(i);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return objects.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private LinearLayout rel_top;
        private RelativeLayout rel_booked_by;
        private CardView cardView;
        private LinearLayout linOrders;
        private TextView txt_LocationDetails;
        private TextView txt_TrialCodedetails;
        private TextView txt_TrialTagDetails;
        private TextView txt_TrialSegmentDetails;
        private Button btnPLD;
        private TextView noorder;
        private RecyclerView recycler;
        ImageView arrow;

        public DataObjectHolder(View view) {
            super(view);
            rel_top = (LinearLayout) view.findViewById(R.id.rel_top);
            cardView = (CardView) view.findViewById(R.id.card_view);
            txt_TrialCodedetails = (TextView) view.findViewById(R.id.txt_TrialCodedetails);
            txt_LocationDetails = (TextView) view.findViewById(R.id.txt_LocationDetails);
            txt_TrialTagDetails = (TextView) view.findViewById(R.id.txt_TrialTagDetails);
            txt_TrialSegmentDetails = (TextView) view.findViewById(R.id.txt_TrialSegmentDetails);
            btnPLD = (Button) view.findViewById(R.id.btnPLD);

        }
    }
}
