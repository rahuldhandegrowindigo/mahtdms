package com.newtrail.mahyco.trail;


import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.widget.RadioButton;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UploadData extends Fragment {


    public UploadData() {
        // Required empty public constructor
    }

    String type = null;
    databaseHelper databaseHelper1;
    ProgressDialog dialog;
    public String SERVER = "http://cmr.mahyco.com/BreaderDataHandler.ashx";
    public Messageclass msclass;
    public CommonExecution cx;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    public String userCode;

    Config config;
    Button btnUpload;
    Button btnUpload2;
    Button btnUploadPLDSown;
    Button btnUploadFeedback;
    TextView txttotalRemain, txttotalRemain1;
    RadioButton rbtsowing, rbtobservation, rnd1, rnd2, rnd3, rndPLD, rbtobservationImage, rndFbk;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_upload_data, container, false);

        rbtsowing = (RadioButton) rootView.findViewById(R.id.rbtsowing);
        rbtobservation = (RadioButton) rootView.findViewById(R.id.rbtobservation);
        rbtobservationImage = (RadioButton) rootView.findViewById(R.id.rbtobservationImage);
        rnd1 = (RadioButton) rootView.findViewById(R.id.rnd1);
        rnd2 = (RadioButton) rootView.findViewById(R.id.rnd2);
        rnd3 = (RadioButton) rootView.findViewById(R.id.rnd3);
        rndPLD = (RadioButton) rootView.findViewById(R.id.rndPLD);
        rndFbk = (RadioButton) rootView.findViewById(R.id.rndFbk);


        btnUpload = (Button) rootView.findViewById(R.id.btnUpload);
        btnUpload2 = (Button) rootView.findViewById(R.id.btnUpload2);
        btnUploadPLDSown = (Button) rootView.findViewById(R.id.btnUploadPLDSown);
        btnUploadFeedback = (Button) rootView.findViewById(R.id.btnUploadFeedback);

        //   txttotalRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        // txttotalRemain1 = (TextView) rootView.findViewById(R.id.txtRemain1);
        //txtTagRemain=(TextView)rootView.findViewById(R.id.txtTagRemain);

        msclass = new Messageclass(this.getContext());
        cx = new CommonExecution(this.getContext());
        config = new Config(this.getContext()); //Here the context is passing
        databaseHelper1 = new databaseHelper(this.getContext());
        dialog = new ProgressDialog(this.getContext());
        dialog.setTitle("Uploading Data");


        recordshow();
        recordshow1();

//        rbtobservation.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                rbtobservation.setChecked(true);
//                rbtsowing.setChecked(false);
//            }
//        });
//
//        rbtsowing.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                rbtsowing.setChecked(true);
//                rbtobservation.setChecked(false);
//            }
//        });

        final Cursor data = databaseHelper1.fetchusercode();

        if (data.getCount() == 0) {
            msclass.showMessage("No Data Available... ");
        } else {
            data.moveToFirst();
            if (data != null) {
                do {
                    userCode = data.getString((data.getColumnIndex("user_code")));
                } while (data.moveToNext());

            }
            data.close();
        }

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rbtsowing.isChecked()) {

                    ///Upload Tag And Sowing Data
                    UploadFarmerData("BreederFarmerDataInsert");
                    UploadTagData("TagData");
                } else if (rbtobservation.isChecked()) {
                    //Upload Observations
                    UploadObservation("UploadObservation");
                } else if (rbtobservationImage.isChecked()) {
                    //Upload Observations
                    new SyncDataAsync_Async("Observationtaken").execute();
                } else {
                    msclass.showMessage("Please Select Any One To Upload");
                }
            }
        });

        btnUpload2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    boolean flag = false;
                    if (recordshowTravel() > 0) {
                        MDO_TravelData();
                        UploadaImage2("mdo_starttravel", cx.MDOurlpath);
                        UploadaImage2("mdo_endtravel", cx.MDOurlpath);

                    } else {
                        msclass.showMessage("Data not available for uploading");
                    }


                } catch (Exception ex) {
                    msclass.showMessage(ex.getMessage());
                    ex.printStackTrace();

                }

            }
        });
        btnUploadPLDSown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    boolean flag = false;
                    if (recordshowPLD() > 0) {
                        new SyncDataAsync_Async("PLDNotSown").execute();

                    } else {
                        msclass.showMessage("Data not available for uploading");
                    }


                } catch (Exception ex) {
                    msclass.showMessage(ex.getMessage());
                    ex.printStackTrace();

                }

            }
        });

        btnUploadFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    boolean flag = false;
                    if (recordshowFeedback() > 0) {
                        uploadFeedbackData("FeedbackTaken", cx.Bredderurlpath);

                    } else {
                        msclass.showMessage("Data not available for uploading");
                    }


                } catch (Exception ex) {
                    msclass.showMessage(ex.getMessage());
                    ex.printStackTrace();

                }

            }
        });
        recordshowPLD();
        recordshowTravel();
        recordshowFeedback();
        //  new ObservationTaken_Async("Observationtaken").execute();
        return rootView;
    }

    class SyncDataAsync_Async extends AsyncTask<Void, Void, String> {
        //  ProgressDialog progressDialog;

        String tag;
        ProgressDialog progressDialog;


        public SyncDataAsync_Async(String tag) {
            this.tag = tag;
        }

        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setCanceledOnTouchOutside(true);

            progressDialog.setCancelable(true);
            // progressDialog.setMessage(getResources().getString(R.string.loading));
            progressDialog.show();

        }

        @Override
        protected String doInBackground(Void... params) {
            if (tag.equals("Observationtaken"))
                uploadObservationImage("UploadImages", cx.Bredderurlpath);
            else if (tag.equals("PLDNotSown")) {
                uploadPLDNotSownData("PLDNotSown", cx.Bredderurlpath);
                uploadPLDImage("UploadImages", cx.Bredderurlpath);
            }
//            else if (tag.equals("FeedbackTaken")) {
//                uploadFeedbackData("FeedbackTaken", cx.Bredderurlpath);
//            }

            return "";
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)

        protected void onPostExecute(String result) {


            if (progressDialog != null) {
                progressDialog.dismiss();
            }

        }
    }

    public void UploadaImage2(String Functionname, String apiUrl) {

        try {
            if (config.NetworkConnection()) {
                // dialog.setMessage("Loading....");
                //dialog.show();
                String str = null;
                String Imagestring1 = "";
                String Imagestring2 = "";
                String ImageName = "";
                String ImageName2 = "tt";
                // str = cx.new MDOMasterData(1, txtUsername.getText().toString(), txtPassword.getText().toString()).execute().get();
                String searchQuery = "select  *  from " + Functionname + " where imgstatus='0'";
                Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
                int count = cursor.getCount();
                if (count > 0) {

                    try {

                        cursor.moveToFirst();
                        while (cursor.isAfterLast() == false) {

                            // for (int i=0; i<count;i++) {

                            //START
                            byte[] objAsBytes = null;//new byte[10000];
                            JSONObject object = new JSONObject();
                            try {
                                ImageName = cursor.getString(cursor.getColumnIndex("imgname"));
                                Imagestring1 = databaseHelper1.getImageDatadetail(cursor.getString(cursor.getColumnIndex("imgpath")));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                objAsBytes = object.toString().getBytes("UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            new UploadImageData(Functionname, Imagestring1, ImageName, "t").execute(apiUrl);


                            cursor.moveToNext();
                        }
                        cursor.close();
                        //End
                  /* if(str.contains("True")) {

                       dialog.dismiss();
                       msclass.showMessage("Records Uploaded successfully");

                       recordshow();
                   }
                   else
                   {
                       msclass.showMessage(str);
                       dialog.dismiss();
                   }
                    */
                    } catch (Exception ex) {  // dialog.dismiss();
                        msclass.showMessage(ex.getMessage());

                    }
                } else {
                    //dialog.dismiss();
                    //msclass.showMessage("Image Data not available for Uploading ");
                    // dialog.dismiss();

                }

            } else {
                msclass.showMessage("Internet network not available.");
                //dialog.dismiss();
            }
            // dialog.dismiss();
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();

        }
    }

    public void uploadObservationImage(String Functionname, String apiUrl) {

        try {
            if (config.NetworkConnection()) {
                // dialog.setMessage("Loading....");
                //dialog.show();
                String str = null;
                String Imagestring1 = "";
                String Imagestring2 = "";
                String ImageName = "";
                String ImageName2 = "tt";
                // str = cx.new MDOMasterData(1, txtUsername.getText().toString(), txtPassword.getText().toString()).execute().get();
                String searchQuery = "select  DISTINCT Image,ImageName  from  Observationtaken  where " +
                        "(Image IS NOT NULL AND Image!='' ) AND ImageSyncStatus='0'";
                Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
                int count = cursor.getCount();
                if (count > 0) {

                    try {

                        cursor.moveToFirst();
                        while (cursor.isAfterLast() == false) {

                            // for (int i=0; i<count;i++) {

                            //START
                            byte[] objAsBytes = null;//new byte[10000];
                            JSONObject object = new JSONObject();
                            try {
                                ImageName = cursor.getString(cursor.getColumnIndex("ImageName"));
                                Imagestring1 = databaseHelper1.getImageDatadetail(cursor.getString(cursor.getColumnIndex("Image")));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                objAsBytes = object.toString().getBytes("UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            syncSingleImage(Functionname, apiUrl, ImageName, Imagestring1);

                            cursor.moveToNext();
                        }
                        cursor.close();

                    } catch (Exception ex) {  // dialog.dismiss();
                        msclass.showMessage(ex.getMessage());

                    }
                } else {

                }

            } else {
                msclass.showMessage("Internet network not available.");
                //dialog.dismiss();
            }
            // dialog.dismiss();
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();

        }
    }

    public void uploadPLDImage(String Functionname, String apiUrl) {

        try {
            if (config.NetworkConnection()) {
                // dialog.setMessage("Loading....");
                //dialog.show();
                String str = null;
                String Imagestring1 = "";
                String Imagestring2 = "";
                String ImageName = "";
                String ImageName2 = "tt";
                // str = cx.new MDOMasterData(1, txtUsername.getText().toString(), txtPassword.getText().toString()).execute().get();
                String searchQuery = "select  DISTINCT imagePath,imageName  from  PLD_NOT_SOWN  where " +
                        "(imagePath IS NOT NULL AND imagePath!='' ) AND imageSyncStatus='0'";
                Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
                int count = cursor.getCount();
                if (count > 0) {

                    try {

                        cursor.moveToFirst();
                        while (cursor.isAfterLast() == false) {

                            // for (int i=0; i<count;i++) {

                            //START
                            byte[] objAsBytes = null;//new byte[10000];
                            JSONObject object = new JSONObject();
                            try {
                                ImageName = cursor.getString(cursor.getColumnIndex("ImageName"));
                                Imagestring1 = databaseHelper1.getImageDatadetail(cursor.getString(cursor.getColumnIndex("imagePath")));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                objAsBytes = object.toString().getBytes("UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            syncSingleImage(Functionname, apiUrl, ImageName, Imagestring1);

                            cursor.moveToNext();
                        }
                        cursor.close();

                    } catch (Exception ex) {  // dialog.dismiss();
                        msclass.showMessage(ex.getMessage());

                    }
                } else {

                }

            } else {
                msclass.showMessage("Internet network not available.");
                //dialog.dismiss();
            }
            // dialog.dismiss();
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();

        }
    }

    public void uploadPLDNotSownData(String Functionname, String apiUrl) {

        try {
            if (config.NetworkConnection()) {

                String Imagestring1 = "";
                String ImageName = "";
                String searchQuery = "select  *  from " + Functionname + " where rowSyncStatus='0'";
                Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
                int count = cursor.getCount();
                if (count > 0) {

                    try {

                        cursor.moveToFirst();
                        while (cursor.isAfterLast() == false) {

                            // for (int i=0; i<count;i++) {

                            //START
                            byte[] objAsBytes = null;//new byte[10000];
                            JSONObject object = new JSONObject();
                            try {
                                if (cursor.getString(cursor.getColumnIndex("imageName")) != null
                                        &&
                                        !cursor.getString(cursor.getColumnIndex("imageName")).contains("null")) {
                                    Imagestring1 = databaseHelper1.getImageDatadetail(cursor.getString(cursor.getColumnIndex("imagePath")));
                                    ImageName = cursor.getString(cursor.getColumnIndex("imageName"));

                                }
                                String userCode = cursor.getString(cursor.getColumnIndex("userCode"));
                                String trialCode = cursor.getString(cursor.getColumnIndex("trialCode"));
                                String status = cursor.getString(cursor.getColumnIndex("status"));
                                String remark = cursor.getString(cursor.getColumnIndex("remark"));
                                String date = cursor.getString(cursor.getColumnIndex("date"));
                                String id = cursor.getString(cursor.getColumnIndex("id"));
                                syncSingleImageAndData(Functionname, apiUrl, ImageName, Imagestring1, id, userCode, trialCode, status
                                        , remark, date);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            cursor.moveToNext();
                        }
                        cursor.close();

                    } catch (Exception ex) {  // dialog.dismiss();
                        msclass.showMessage(ex.getMessage());

                    }
                } else {

                }

            } else {
                msclass.showMessage("Internet network not available.");
                //dialog.dismiss();
            }
            // dialog.dismiss();
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();

        }
    }

    public void uploadFeedbackData(String Functionname, String apiUrl) {
        String str = "";
        try {
            if (Config.NetworkConnection()) {


                String searchQuery = " select * from " + Functionname + " where isSyncedStatus='0' and isSubmitted= '1' ";
                Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
                int count = cursor.getCount();
//


                JSONArray jsonArray = new JSONArray();
                if (count > 0) {

                    try {

                        JSONObject jsonObject = new JSONObject();

                        try {
                            jsonArray = databaseHelper1.getResultsFeedback(searchQuery, userCode);
                            jsonObject.put("Table", jsonArray);
                            str = new UploadFeedbackData(Functionname, jsonObject).execute(apiUrl).get();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        cursor.close();
                        if (str.contains("True")) {


                            if (Functionname.equals("FeedbackTaken")) {


                                for (int i = 0; i < jsonArray.length(); i++) {
                                    databaseHelper1.Updatedata("update FeedbackTaken  set isSyncedStatus='1'");
                                }


                            }
                            msclass.showMessage("Data uploaded successfully.");
                            recordshowFeedback();
                            dialog.dismiss();

                        } else {
                            msclass.showMessage(str + "-E");

                            dialog.dismiss();
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();


                    }
                } else {
                    dialog.dismiss();
                    msclass.showMessage("Data not available for Uploading ");


                }
            } else {
                msclass.showMessage("Internet network not available.");
                dialog.dismiss();
            }
            dialog.dismiss();
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();

        }
    }


    public void MDO_TravelData() {
        //if(config.NetworkConnection()) {
        // dialog.setMessage("Loading. Please wait...");
        //dialog.show();
        String str = null;
        String Imagestring1 = "";
        String Imagestring2 = "";
        String ImageName = "";
        Cursor cursor = null;
        String searchQuery = "";
        int count = 0;
        searchQuery = "select * from mdo_starttravel where Status='0'";
        cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
        count = cursor.getCount();
        cursor.close();

        searchQuery = "select * from mdo_endtravel where Status='0'";
        cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
        count = count + cursor.getCount();
        cursor.close();

        searchQuery = "select * from mdo_addplace where Status='0'";
        cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
        count = count + cursor.getCount();
        cursor.close();

//        searchQuery = "select * from mdo_Retaileranddistributordata where Status='0'";
//        cursor = mDatabase.getReadableDatabase().rawQuery(searchQuery, null );
//        count=count+cursor.getCount();
//        cursor.close();


        if (count > 0) {
            try {
                //START
                byte[] objAsBytes = null;//new byte[10000];
                JSONObject object = new JSONObject();
                try {
                    searchQuery = "select * from mdo_starttravel where Status='0'";
                    object.put("Table1", databaseHelper1.getResults(searchQuery));
                    searchQuery = "select * from mdo_endtravel where Status='0'";
                    object.put("Table2", databaseHelper1.getResults(searchQuery));
                    searchQuery = "select * from mdo_addplace where Status='0'";
                    object.put("Table3", databaseHelper1.getResults(searchQuery));
                    // searchQuery = "select * from mdo_Retaileranddistributordata where Status='0'";
                    object.put("Table4", new JSONArray());
                    //searchQuery = "select * from mdo_retailerproductdetail where Status='0'";
                    object.put("Table5", new JSONArray());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    objAsBytes = object.toString().getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                new UploadDataServernew("MDO_TravelData", objAsBytes, Imagestring1, Imagestring2, ImageName, "").execute(cx.MDOurlpath);

            } catch (Exception ex) {
                // msclass.showMessage(ex.getMessage());

            }
        } else {
            // msclass.showMessage("Uploading data not available");

        }

    }

    private int recordshowTravel() {
        int totalcount = 0;
        try {
            String searchQuery = "";


            int count2 = 0;
            searchQuery = "select * from mdo_starttravel where Status='0'";
            Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
            count2 = cursor.getCount();
            cursor.close();
            totalcount = totalcount + count2;

            searchQuery = "select * from mdo_endtravel where Status='0'";
            cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
            count2 = count2 + cursor.getCount();
            cursor.close();
            totalcount = totalcount + count2;
            searchQuery = "select * from mdo_addplace where Status='0'";
            cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
            count2 = count2 + cursor.getCount();
            cursor.close();
            rnd1.setText("Pending my travel data=" + String.valueOf(count2));
            totalcount = totalcount + count2;
            count2 = 0;
            searchQuery = "select * from mdo_starttravel where imgstatus='0'";
            cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
            count2 = count2 + cursor.getCount();
            cursor.close();
            searchQuery = "select * from mdo_endtravel where imgstatus='0'";
            cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
            count2 = count2 + cursor.getCount();
            cursor.close();
            totalcount = totalcount + count2;
            rnd3.setText("My travel start and end vehicle reading images =" + String.valueOf(count2));

            ;

        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();
            dialog.dismiss();
        }
        return totalcount;
    }


    private int recordshowPLD() {
        int totalcount = 0;
        try {
            String searchQuery = "";


            int count2 = 0;
            searchQuery = "select * from PLDNotSown where rowSyncStatus='0'";
            Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
            count2 = cursor.getCount();
            cursor.close();
            totalcount = totalcount + count2;
            rndPLD.setText("PLD Sown record  =" + String.valueOf(totalcount));


        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();
            dialog.dismiss();
        }
        return totalcount;
    }

    private int recordshowFeedback() {
        int totalcount = 0;
        try {
            String searchQuery = "";


            int count2 = 0;
            searchQuery = "select * from FeedbackTaken where isSyncedStatus='0' and isSubmitted='1' ";
            Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
            count2 = cursor.getCount();
            cursor.close();
            totalcount = totalcount + count2;
            rndFbk.setText("Feedback records  =" + String.valueOf(totalcount));


        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
            ex.printStackTrace();
            dialog.dismiss();
        }
        return totalcount;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        androidx.appcompat.app.ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle("Upload Data");
    }

    private void recordshow() {
        String searchQuery = "select  *  from FarmerInfodata inner join tagdata1 on FarmerInfodata.TRIL_CODE=tagdata1.TRIL_CODE where tagdata1.flag='T' and tagdata1.Upload='U'";
        Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
        int count = cursor.getCount();
        rbtsowing.setText("Pending Sowing & Tag Data = " + String.valueOf(count));
        cursor.close();

    }

    private void recordshow1() {
        String searchQuery = "select  distinct PlotNo  from Observationtaken where flag='0'";
        Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
        int count1 = cursor.getCount();
        rbtobservation.setText("Pending Plot To Upload = " + String.valueOf(count1));
        cursor.close();
        String searchQuery1 = "select  DISTINCT Image,ImageName  from Observationtaken where " +
                "(Image IS NOT NULL AND Image!='' ) AND ImageSyncStatus='0'";
        Cursor cursor1 = databaseHelper1.getReadableDatabase().rawQuery(searchQuery1, null);
        int count = cursor.getCount();
        cursor1.close();
        rbtobservationImage.setText("Pending Observation Images To Upload = " + String.valueOf(count));

    }

    public void UploadFarmerData(String BreederFarmerDataInsert) {
        if (Config.NetworkConnection()) {
            dialog.setMessage("Loading....");
            dialog.show();
            String str = null;
            // str = cx.new MDOMasterData(1, txtUsername.getText().toString(), txtPassword.getText().toString()).execute().get();
            String searchQuery = "select  *  from FarmerInfodata where flag='0'";
            Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
            int count = cursor.getCount();

            if (count > 0) {

                try {
                    byte[] objAsBytes = null;//new byte[10000];
                    JSONObject object = new JSONObject();
                    try {
                        object.put("Table1", databaseHelper1.getResults(searchQuery));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        objAsBytes = object.toString().getBytes("UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    dialog.setMessage("Loading. Please wait...");
                    dialog.show();
                    str = new UploadDataServer(BreederFarmerDataInsert, objAsBytes).execute(SERVER).get();
                    ;
                    //End
                    cursor.close();
                    //End
                    if (str.contains("True")) {

                        dialog.dismiss();
                        msclass.showMessage("Records Uploaded successfully");
                        String searchQuery1 = "update FarmerInfodata set flag = '1' where flag='0'";
                        databaseHelper1.runQuery(searchQuery1);

                        recordshow();
                    } else {
                        msclass.showMessage(str);
                        dialog.dismiss();
                    }

                } catch (Exception ex) {
                    dialog.dismiss();
                    msclass.showMessage(ex.getMessage());


                }
            } else {
                dialog.dismiss();
                msclass.showMessage("Data not available for Uploading ");
                dialog.dismiss();

            }

        } else {
            msclass.showMessage("Internet network not available.");
            dialog.dismiss();
        }
        //dialog.dismiss();
    }

    // To Upload Obseervation On server
    public void UploadObservation(String UploadObservation) {
        if (config.NetworkConnection()) {
            dialog.setMessage("Loading....");
            dialog.show();
            String str = null;
            // str = cx.new MDOMasterData(1, txtUsername.getText().toString(), txtPassword.getText().toString()).execute().get();
            String searchQuery = "select  *  from Observationtaken where flag='0'";
            Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
            int count = cursor.getCount();

            if (count > 0) {

                try {
                    byte[] objAsBytes = null;//new byte[10000];
                    JSONObject object = new JSONObject();
                    try {
                        object.put("Table1", databaseHelper1.getResults(searchQuery));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        objAsBytes = object.toString().getBytes("UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    dialog.setMessage("Loading. Please wait...");
                    dialog.show();
                    str = new UploadDataServer(UploadObservation, objAsBytes).execute(SERVER).get();
                    ;
                    //End
                    cursor.close();
                    //End
                    if (str.contains("True")) {

                        dialog.dismiss();
                        msclass.showMessage("Records Uploaded successfully");
                        String searchQuery1 = "update observationtaken set flag = '1' where flag='0'";
                        databaseHelper1.runQuery(searchQuery1);

                        recordshow1();
                    } else {
                        msclass.showMessage(str);
                        dialog.dismiss();
                    }

                } catch (Exception ex) {
                    dialog.dismiss();
                    msclass.showMessage(ex.getMessage());


                }
            } else {
                dialog.dismiss();
                msclass.showMessage("Data not available for Uploading ");
                dialog.dismiss();

            }

        } else {
            msclass.showMessage("Internet network not available.");
            dialog.dismiss();
        }
        //dialog.dismiss();
    }
    //

    public void UploadTagData(String TagData) {
        if (config.NetworkConnection()) {
            dialog.setMessage("Loading....");
            dialog.show();
            String str = null;
            // str = cx.new MDOMasterData(1, txtUsername.getText().toString(), txtPassword.getText().toString()).execute().get();

            String searchQuery = "select  *  from tagdata1 where flag='T' and Upload='U'";
            Cursor cursor = databaseHelper1.getReadableDatabase().rawQuery(searchQuery, null);
            int count = cursor.getCount();

            if (count > 0) {

                try {
                    byte[] objAsBytes = null;//new byte[10000];
                    JSONObject object = new JSONObject();
                    try {
                        object.put("Table1", databaseHelper1.getResults(searchQuery));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        objAsBytes = object.toString().getBytes("UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    dialog.setMessage("Loading. Please wait...");
                    dialog.show();
                    str = new UploadDataServer(TagData, objAsBytes).execute(SERVER).get();
                    ;
                    //End
                    cursor.close();
                    //End
                    if (str.contains("True")) {

                        dialog.dismiss();
                        msclass.showMessage("Tag Data Uploaded successfully");
                        String searchQuery1 = "update tagdata set Uplaod = 'Y' where flag='T' and Uplaod='U'";
                        databaseHelper1.runQuery(searchQuery1);

                        String searchQuery2 = "update tagdata1 set Upload = 'Y' where flag='T'and Upload='U'";
                        databaseHelper1.runQuery(searchQuery2);

                        recordshow();
                    } else {
                        msclass.showMessage(str);
                        dialog.dismiss();
                    }

                } catch (Exception ex) {
                    dialog.dismiss();
                    msclass.showMessage(ex.getMessage());


                }
            } else {
                dialog.dismiss();
                msclass.showMessage("Data not available for Uploading ");
                dialog.dismiss();

            }

        } else {
            msclass.showMessage("Internet network not available.");
            dialog.dismiss();
        }
        //dialog.dismiss();
    }

    public class UploadDataServer extends AsyncTask<String, String, String> {

        byte[] objAsBytes;
        String Imagestring1;
        String Imagestring2;
        String ImageName;
        String Funname;

        public UploadDataServer(String Funname, byte[] objAsBytes) {

            //this.IssueID=IssueID;
            this.objAsBytes = objAsBytes;
            this.Imagestring1 = Imagestring1;
            this.Imagestring2 = Imagestring2;
            this.ImageName = ImageName;
            this.Funname = Funname;

        }

        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... urls) {

            // encode image to base64 so that it can be picked by saveImage.php file
            String encodeImage = Base64.encodeToString(objAsBytes, Base64.DEFAULT);
            //showMessage(encodeImage);
            HttpClient httpclient = new DefaultHttpClient();
            StringBuilder builder = new StringBuilder();
            List<NameValuePair> postParameters = new ArrayList<NameValuePair>(2);
            postParameters.add(new BasicNameValuePair("from", Funname));
            postParameters.add(new BasicNameValuePair("encodedData", encodeImage));

            String Urlpath = urls[0];

            // String Urlpath=urls[0]+"?action=2&farmerid="+userID+"&croptype="+croptype+"&imagename=Profile.png&issueDescription="+IssueDesc+"&issueid=1";

            HttpPost httppost = new HttpPost(Urlpath);
            httppost.addHeader("Content-type", "application/x-www-form-urlencoded");

            try {
                httppost.setEntity(new UrlEncodedFormEntity(postParameters));
                UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
                httppost.setEntity(formEntity);

                HttpResponse response = httpclient.execute(httppost);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }

                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                msclass.showMessage(e.getMessage().toString());
                dialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                msclass.showMessage(e.getMessage().toString());
                dialog.dismiss();
            }

            //dialog.dismiss();
            return builder.toString();
        }

        protected void onPostExecute(String result) {
            String weatherInfo = "Weather Report  is: \n";
            try {
                String resultout = result.trim();
                if (resultout.equals("True")) {
                    // msclass.showMessage("Data uploaded successfully.");

                    if (Funname.equals("BreederFarmerDataInsert")) {

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Date d = new Date();
                        String strdate = dateFormat.format(d);
                        // String searchQuery = "select * from TagData  where  strftime( '%Y-%m-%d', INTime)<>'"+strdate+"'  ";
                        // mDatabase.deleterecord("delete from TagData where strftime( '%Y-%m-%d', INTime)<>'"+strdate+"' ");
                        // mDatabase.Updatedata("update TagData  set Status='1' where Imgname='"+ImageName+"'");
                        recordshow();
                    }

                } else {
                    msclass.showMessage(result + "error");
                }

                // dialog.dismiss();


            } catch (Exception e) {
                e.printStackTrace();
                msclass.showMessage(e.getMessage().toString());
                dialog.dismiss();
            }

        }
    }

    public class UploadFeedbackData extends AsyncTask<String, String, String> {
        ProgressDialog pd;

        JSONObject obj;
        String Funname;


        public UploadFeedbackData(String Funname, JSONObject obj) {

            this.obj = obj;
            this.Funname = Funname;
        }

        protected void onPreExecute() {

//            pd = new ProgressDialog(getActivity());
//            pd.setTitle("Data Uploading ...");
//            pd.setMessage("Please wait.");
//            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            pd.show();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            byte[] objAsBytes = null;//new byte[10000];
            try {
                objAsBytes = obj.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String encodeImage = Base64.encodeToString(objAsBytes, Base64.DEFAULT);

            HttpClient httpclient = new DefaultHttpClient();
            StringBuilder builder = new StringBuilder();
            List<NameValuePair> postParameters = new ArrayList<NameValuePair>(2);
            postParameters.add(new BasicNameValuePair("from", "FeedbackTaken"));
            postParameters.add(new BasicNameValuePair("FeedbackData",encodeImage));
            Log.d("RequestuploadFeedback", postParameters + "");

            String Urlpath =urls[0];
            Log.d("uploadFeedbackData", Urlpath);


            HttpPost httppost = new HttpPost(Urlpath);
            httppost.addHeader("Content-type", "application/x-www-form-urlencoded");

            try {
                httppost.setEntity(new UrlEncodedFormEntity(postParameters));
                UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
                httppost.setEntity(formEntity);

                HttpResponse response = httpclient.execute(httppost);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }

                }
                //   pd.dismiss();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                //   pd.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                //   pd.dismiss();
            }

            //pd.dismiss();
            return builder.toString();
        }

        protected void onPostExecute(String result) {
            try {
                String resultout = result.trim();
                Log.d("Upload", "onPostExecute: " + resultout);
                dialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                dialog.dismiss();
            }

        }
    }

    public class UploadImageData extends AsyncTask<String, String, String> { //MyTravel
        ProgressDialog pd;
        byte[] objAsBytes;
        String Imagestring1;
        String Imagestring2;
        String ImageName, ImageName2;
        String Funname, Intime;

        public UploadImageData(String Funname, String Imagestring1, String ImageName, String Intime) {

            //this.IssueID=IssueID;
            this.objAsBytes = objAsBytes;
            this.Imagestring1 = Imagestring1;
            this.Imagestring2 = Imagestring2;
            this.ImageName = ImageName;
            this.ImageName2 = ImageName2;
            this.Funname = Funname;
            this.Intime = Intime;

        }

        protected void onPreExecute() {

            pd = new ProgressDialog(getActivity());
            pd.setTitle("Data Uploading ...");
            pd.setMessage("Please wait.");
            // pd.setCancelable(false);
            // pd.setIndeterminate(true);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(String... urls) {

            // encodeImage = Base64.encodeToString(objAsBytes,Base64.DEFAULT);
            HttpClient httpclient = new DefaultHttpClient();
            StringBuilder builder = new StringBuilder();
            List<NameValuePair> postParameters = new ArrayList<NameValuePair>(2);
            postParameters.add(new BasicNameValuePair("Type", "UploadImages"));
            //postParameters.add(new BasicNameValuePair("encodedData", encodeImage));
            postParameters.add(new BasicNameValuePair("input1", Imagestring1));
            postParameters.add(new BasicNameValuePair("input2", Imagestring2));

            //String Urlpath=urls[0];

            String Urlpath = urls[0] + "?ImageName=" + ImageName + "&ImageName2=" + ImageName2;
            Log.d("rohit", "doInBackground: " + Urlpath);
            Log.d("rohit", "doInBackground:params::: " + postParameters);
            HttpPost httppost = new HttpPost(Urlpath);
            httppost.addHeader("Content-type", "application/x-www-form-urlencoded");

            try {
                httppost.setEntity(new UrlEncodedFormEntity(postParameters));
                UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
                httppost.setEntity(formEntity);

                HttpResponse response = httpclient.execute(httppost);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }

                }
                pd.dismiss();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                // msclass.showMessage(e.getMessage().toString());
                pd.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                // msclass. showMessage(e.getMessage().toString());
                pd.dismiss();
            }

            pd.dismiss();
            return builder.toString();
        }

        protected void onPostExecute(String result) {
            String weatherInfo = "Weather Report  is: \n";
            try {
                String resultout = result.trim();
                if (resultout.contains("True")) {
                    // msclass.showMessage("Data uploaded successfully.");
                    pd.dismiss();

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date d = new Date();
                    String strdate = dateFormat.format(d);
                    if (Funname.equals("mdo_starttravel")) {
                        databaseHelper1.Updatedata("update mdo_starttravel  set imgstatus='1' where imgname='" + ImageName + "'");
                        pd.dismiss();
                    }
                    if (Funname.equals("mdo_endtravel")) {
                        databaseHelper1.Updatedata("update mdo_endtravel  set imgstatus='1' where imgname='" + ImageName + "'");
                        databaseHelper1.Updatedata("delete from mdo_starttravel  where  Status='1' and strftime( '%Y-%m-%d', startdate)<>'" + strdate + "'");
                        databaseHelper1.Updatedata("delete from  mdo_endtravel  where  Status='1' and strftime( '%Y-%m-%d', enddate)<>'" + strdate + "'");
                        databaseHelper1.Updatedata("delete from  mdo_addplace  where  Status='1' and strftime( '%Y-%m-%d', date)<>'" + strdate + "'");

                        pd.dismiss();
                    }
                    if (Funname.equals("Observationtaken")) {
                        databaseHelper1.Updatedata("update Observationtaken  set ImageSyncStatus='1' where ImageName='" + ImageName + "'");
                        pd.dismiss();
                    }

                } else {
                    msclass.showMessage(result + "--E");
                    pd.dismiss();
                }

                pd.dismiss();

                recordshowTravel();
            } catch (Exception e) {
                e.printStackTrace();
                // msclass.showMessage(e.getMessage().toString());
                pd.dismiss();
            }

        }
    }

    public synchronized void syncSingleImage(String function, String urls, String ImageName, String Imagestring1) {
        HttpClient httpclient = new DefaultHttpClient();
        StringBuilder builder = new StringBuilder();
        List<NameValuePair> postParameters = new ArrayList<NameValuePair>(2);
        postParameters.add(new BasicNameValuePair("from", "UploadImages"));
        postParameters.add(new BasicNameValuePair("input1", Imagestring1));


        String Urlpath = urls + "?ImageName=" + ImageName;
        Log.d("rohit", "doInBackground: " + Urlpath);
        Log.d("rohit", "doInBackground:params::: " + postParameters);
        HttpPost httppost = new HttpPost(Urlpath);
        httppost.addHeader("Content-type", "application/x-www-form-urlencoded");

        try {
            httppost.setEntity(new UrlEncodedFormEntity(postParameters));
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
            httppost.setEntity(formEntity);

            HttpResponse response = httpclient.execute(httppost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));

                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line).append("\n");
                }

            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
            // msclass.showMessage(e.getMessage().toString());


        } catch (Exception e) {
            e.printStackTrace();
            // msclass. showMessage(e.getMessage().toString());

        }

        try {
            handleImageSyncResponse(function, builder.toString().trim(), ImageName, "");

        } catch (Exception e) {
            e.printStackTrace();
            // msclass.showMessage(e.getMessage().toString());

        }
    }

    public synchronized void syncSingleImageAndData(String function, String urls, String ImageName
            , String Imagestring1
            , String id
            , String userCode
            , String trialCode
            , String status
            , String remark
            , String date
    ) {
        HttpClient httpclient = new DefaultHttpClient();
        StringBuilder builder = new StringBuilder();
        List<NameValuePair> postParameters = new ArrayList<NameValuePair>(2);
        postParameters.add(new BasicNameValuePair("from", "PLDNotSown"));
        postParameters.add(new BasicNameValuePair("ImageName", ImageName));
        postParameters.add(new BasicNameValuePair("id", id));
        postParameters.add(new BasicNameValuePair("userCode", userCode));
        postParameters.add(new BasicNameValuePair("trialCode", trialCode));
        postParameters.add(new BasicNameValuePair("status", status));
        postParameters.add(new BasicNameValuePair("remark", remark));
        postParameters.add(new BasicNameValuePair("date", date));
        String Urlpath = urls;//+ "?ImageName=" + ImageName;
        Log.d("rohit", "doInBackground: " + Urlpath);
        Log.d("rohit", "doInBackground:params::: " + postParameters);
        HttpPost httppost = new HttpPost(Urlpath);
        httppost.addHeader("Content-type", "application/x-www-form-urlencoded");

        try {
            httppost.setEntity(new UrlEncodedFormEntity(postParameters));
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
            httppost.setEntity(formEntity);

            HttpResponse response = httpclient.execute(httppost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));

                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line).append("\n");
                }

            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
            // msclass.showMessage(e.getMessage().toString());


        } catch (Exception e) {
            e.printStackTrace();
            // msclass. showMessage(e.getMessage().toString());

        }

        try {
            handleImageSyncResponse(function, builder.toString().trim(), ImageName, id);

        } catch (Exception e) {
            e.printStackTrace();
            // msclass.showMessage(e.getMessage().toString());

        }
    }


    public class UploadDataServernew extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        byte[] objAsBytes;
        String Imagestring1;
        String Imagestring2;
        String ImageName;
        String Funname, Intime;

        public UploadDataServernew(String Funname, byte[] objAsBytes, String Imagestring1, String Imagestring2, String ImageName, String Intime) {

            //this.IssueID=IssueID;
            this.objAsBytes = objAsBytes;
            this.Imagestring1 = Imagestring1;
            this.Imagestring2 = Imagestring2;
            this.ImageName = ImageName;
            this.Funname = Funname;
            this.Intime = Intime;

        }

        protected void onPreExecute() {

            pd = new ProgressDialog(getActivity());
            pd.setTitle("Data Uploading ...");
            pd.setMessage("Please wait.");
            // pd.setCancelable(false);
            // pd.setIndeterminate(true);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(String... urls) {

            String encodeImage = Base64.encodeToString(objAsBytes, Base64.DEFAULT);
            HttpClient httpclient = new DefaultHttpClient();
            StringBuilder builder = new StringBuilder();
            List<NameValuePair> postParameters = new ArrayList<NameValuePair>(2);
            postParameters.add(new BasicNameValuePair("Type", Funname)); // For MyTravel
            postParameters.add(new BasicNameValuePair("encodedData", encodeImage));
            postParameters.add(new BasicNameValuePair("input1", Imagestring1));
            postParameters.add(new BasicNameValuePair("input2", Imagestring2));

            String Urlpath = urls[0];

            // String Urlpath=urls[0]+"?action=2&farmerid="+userID+"&croptype="+croptype+"&imagename=Profile.png&issueDescription="+IssueDesc+"&issueid=1";

            HttpPost httppost = new HttpPost(Urlpath);
            httppost.addHeader("Content-type", "application/x-www-form-urlencoded");

            try {
                httppost.setEntity(new UrlEncodedFormEntity(postParameters));
                UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
                httppost.setEntity(formEntity);

                HttpResponse response = httpclient.execute(httppost);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }

                }
                pd.dismiss();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                // msclass.showMessage(e.getMessage().toString());
                pd.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                // msclass. showMessage(e.getMessage().toString());
                pd.dismiss();
            }

            pd.dismiss();
            return builder.toString();
        }

        protected void onPostExecute(String result) {
            String weatherInfo = "Weather Report  is: \n";
            try {
                String resultout = result.trim();
                Log.d("Upload", "onPostExecute: " + resultout);
                pd.dismiss();
                if (resultout.contains("True")) {
                    // msclass.showMessage("Data uploaded successfully.");
                    pd.dismiss();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date d = new Date();
                    String strdate = dateFormat.format(d);

                    if (Funname.equals("MDO_TravelData")) {
                        databaseHelper1.Updatedata("update mdo_starttravel  set Status='1'");
                        databaseHelper1.Updatedata("update mdo_endtravel  set Status='1'");
                        databaseHelper1.Updatedata("update mdo_addplace  set Status='1'");
                        //   databaseHelper1.Updatedata("delete from mdo_starttravel  where  Status='1' and strftime( '%Y-%m-%d', startdate)<>'" + strdate + "'");
                        //   databaseHelper1.Updatedata("delete from  mdo_endtravel  where  Status='1' and strftime( '%Y-%m-%d', enddate)<>'" + strdate + "'");
                        //   databaseHelper1.Updatedata("delete from  mdo_addplace  where  Status='1' and strftime( '%Y-%m-%d', date)<>'" + strdate + "'");


                        //   pd.dismiss();
                    }
                    recordshowTravel();
                    msclass.showMessage("data uploaded successfully.");
                    pd.dismiss();
                } else {
                    msclass.showMessage(result + "--E");
                    pd.dismiss();
                }
                pd.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                // msclass.showMessage(e.getMessage().toString());
                pd.dismiss();
            }

        }
    }


    public void handleImageSyncResponse(String function, String resultout, String ImageName, String id) {
        if (function.equals("Observationtaken")) {
            if (resultout.contains("True")) {
                databaseHelper1.Updatedata("update Observationtaken  set ImageSyncStatus='1' where ImageName='" + ImageName + "'");

            } else {
                msclass.showMessage(resultout + "Observationtaken--E");

            }
        }
        if (function.equals("PLDNotSown")) {
            if (resultout.contains("True")) {
                if (!ImageName.equals("")) {
                    databaseHelper1.Updatedata("update PLDNotSown  set imageSyncStatus='1',rowSyncStatus='1' where imageName='" + ImageName + "'");

                } else {
                    databaseHelper1.Updatedata("update PLDNotSown  set rowSyncStatus='1' where id='" + id + "'");

                }

            } else {
                msclass.showMessage(resultout + "PLDNotSown--E");

            }
        }
        if (function.equals("FeedbackTaken")) {
            if (resultout.contains("True")) {
                if (!ImageName.equals("")) {
                    databaseHelper1.Updatedata("update FeedbackTaken  set imageSyncStatus='1',rowSyncStatus='1' where imageName='" + ImageName + "'");

                } else {
                    databaseHelper1.Updatedata("update FeedbackTaken  set rowSyncStatus='1' where id='" + id + "'");

                }

            } else {
                msclass.showMessage(resultout + "FeedbackTaken--E");

            }
        }

        Log.d("rohitt", "syncSingleImage: " + resultout);
    }

    public class UploadMultipleImages extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        byte[] objAsBytes;
        String Imagestring1;
        String Imagestring2;
        String ImageName, ImageName2;
        String Funname, Intime;

        public UploadMultipleImages(String Funname, String Imagestring1, String Imagestring2, String ImageName, String ImageName2, String Intime) {

            //this.IssueID=IssueID;
            this.objAsBytes = objAsBytes;
            this.Imagestring1 = Imagestring1;
            this.Imagestring2 = Imagestring2;
            this.ImageName = ImageName;
            this.ImageName2 = ImageName2;
            this.Funname = Funname;
            this.Intime = Intime;

        }


        @Override
        protected String doInBackground(String... urls) {

            // encodeImage = Base64.encodeToString(objAsBytes,Base64.DEFAULT);
            HttpClient httpclient = new DefaultHttpClient();
            StringBuilder builder = new StringBuilder();
            List<NameValuePair> postParameters = new ArrayList<NameValuePair>(2);
            postParameters.add(new BasicNameValuePair("from", "UploadImages"));
            //postParameters.add(new BasicNameValuePair("encodedData", encodeImage));
            postParameters.add(new BasicNameValuePair("input1", Imagestring1));
            postParameters.add(new BasicNameValuePair("input2", Imagestring2));

            //String Urlpath=urls[0];

            String Urlpath = urls[0] + "?ImageName=" + ImageName + "&ImageName2=" + ImageName2;
            Log.d("rohit", "doInBackground: " + Urlpath);
            Log.d("rohit", "doInBackground:params::: " + postParameters);
            HttpPost httppost = new HttpPost(Urlpath);
            httppost.addHeader("Content-type", "application/x-www-form-urlencoded");

            try {
                httppost.setEntity(new UrlEncodedFormEntity(postParameters));
                UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
                httppost.setEntity(formEntity);

                HttpResponse response = httpclient.execute(httppost);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }

                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();
                // msclass.showMessage(e.getMessage().toString());


            } catch (Exception e) {
                e.printStackTrace();
                // msclass. showMessage(e.getMessage().toString());

            }

            try {
                String resultout = builder.toString().trim();
                if (resultout.contains("True")) {
                    if (Funname.equals("Observationtaken")) {
                        databaseHelper1.Updatedata("update Observationtaken  set ImageSyncStatus='1' where ImageName='" + ImageName + "'");
                        pd.dismiss();
                    }

                } else {
                    msclass.showMessage(resultout + "--E");

                }


            } catch (Exception e) {
                e.printStackTrace();
                // msclass.showMessage(e.getMessage().toString());

            }
            return builder.toString();
        }


    }
}
