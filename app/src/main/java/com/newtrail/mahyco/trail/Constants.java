package com.newtrail.mahyco.trail;

public class Constants {

    public static  final String KEY_PLOT_NO="plot_no";
    public static  final String KEY_TRIAL_NO="trial_no";
    public static final float  KEY_KM_RANGE=50.0f;
    public static final float  KEY_METER_RANGE=50000.0f;
}
