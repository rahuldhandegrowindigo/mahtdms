package com.newtrail.mahyco.trail;

import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {
    public android.widget.ProgressBar mprogresss;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    databaseHelper databaseHelper1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        getSupportActionBar().setTitle("Mahyco TDMS");
//        getSupportActionBar().setSubtitle("Trial Data Management System");
        databaseHelper1 = new databaseHelper(this);
        mprogresss = (ProgressBar) findViewById(R.id.progressBar);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        new Thread(new Runnable() {
            @Override
            public void run() {
                dowork();
                startapp();
                finish();
            }
        }).start();

    }

    private void dowork() {

        for (int progress = 0; progress < 100; progress += 10) {
            try {
                Thread.sleep(300);//300
                mprogresss.setProgress(progress);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void startapp() {
        if (pref.getString("UserID", null) != null||pref.getString("USER_EMAIL", null) != null) {
            Intent intent = new Intent(this, home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }else {
            Intent intent = new Intent(this, login.class);
            startActivity(intent);
        }


//        if (pref.getBoolean("loginState",false)) {
//            Intent intent = new Intent(this, home.class);
//            startActivity(intent);
//        }else {
//            Intent intent = new Intent(this, login.class);
//            startActivity(intent);
//        }
    }

}