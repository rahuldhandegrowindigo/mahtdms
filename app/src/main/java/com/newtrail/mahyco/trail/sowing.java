package com.newtrail.mahyco.trail;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Geocoder;
import android.location.Location;
////import com.google.android.gms.location.LocationListener;
import android.os.Bundle;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;


import android.location.LocationListener;
import android.location.LocationManager;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

//import com.google.android.gms.location.LocationListener;

import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */
public class sowing extends Fragment implements LocationListener {


    public sowing() {
        // Required empty public constructor
    }


    EditText txtSowingDate, txtFname, txtFvillage, txtFmobile, txtStartnote, txtArea, txtGeoVillage;
    Calendar myCalendar;
    TextView Trialcodedata, txtNoRows, txtRowLength, txtRRSpecing, txtPPSpacing, txtReplication, txtTotEntry, txtLocation, txtplotstart,txtPlotSize,txtnurseryDate,txtProduct;

    TextView  lblRowLength,lblRRSpecing,lblPPSpacing,lblReplication,lblPlotSize,lbl4,lbl5,lbl6,lbl7,lbl11,lbl12,lblProduct;
    databaseHelper databaseHelper1;
    public Messageclass msclass;
    String txtReplec, NoRows, RowLength, RRSpecing, PPSpacing, Replication, TotEntry, Fname, Fvillage, Fmobile, FstartNote, Fsowingdate, Location, plotstart, Plotend,trial_type,PlotSize,nursery,nurseryDate,product;
    Button Btnsave, BtnFieldTag,BtnUpdate;
    public String userCode;
    public Context cx;
    LocationManager locationManager;

    private double longitude;
    private double latitude;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sowing, container, false);


        final Calendar myCalendar = Calendar.getInstance();
        txtSowingDate = (EditText) rootView.findViewById(R.id.txtSowingDate);
        txtFname = (EditText) rootView.findViewById(R.id.txtFarmerName);
        txtFvillage = (EditText) rootView.findViewById(R.id.txtVillage);
        txtFmobile = (EditText) rootView.findViewById(R.id.txtContact);
        txtStartnote = (EditText) rootView.findViewById(R.id.txtStartNote);
        txtArea = (EditText) rootView.findViewById(R.id.txtArea);
        txtGeoVillage = (EditText) rootView.findViewById(R.id.txtGeoVillage);
        txtnurseryDate=(EditText)rootView.findViewById(R.id.txtNursaryDate);


        txtNoRows = (TextView) rootView.findViewById(R.id.txtrow);
        txtRowLength = (TextView) rootView.findViewById(R.id.txtrowlenght);//
        txtRRSpecing = (TextView) rootView.findViewById(R.id.txtrrSpecing);//
        txtPPSpacing = (TextView) rootView.findViewById(R.id.txtppSpecing);//
        txtReplication = (TextView) rootView.findViewById(R.id.txtreplec); //
        txtTotEntry = (TextView) rootView.findViewById(R.id.txttotEntry);
        txtLocation = (TextView) rootView.findViewById(R.id.txtLocation);
        txtplotstart = (TextView) rootView.findViewById(R.id.txtplotstart);
        txtPlotSize=(TextView)rootView.findViewById(R.id.txtPlotSize); //
        txtProduct=(TextView)rootView.findViewById(R.id.txtProduct);

        lbl4=(TextView)rootView.findViewById(R.id.lbl4);
        lbl5=(TextView)rootView.findViewById(R.id.lbl5);
        lbl6=(TextView)rootView.findViewById(R.id.lbl6);
        lbl7=(TextView)rootView.findViewById(R.id.lbl7);
        lbl11=(TextView)rootView.findViewById(R.id.lbl11);
        lbl12=(TextView)rootView.findViewById(R.id.lbl12);

        lblRowLength=(TextView)rootView.findViewById(R.id.lblrowlenght);
        lblRRSpecing=(TextView)rootView.findViewById(R.id.lblrrSpecing);
        lblPPSpacing=(TextView)rootView.findViewById(R.id.lblppSpecing);
        lblReplication=(TextView)rootView.findViewById(R.id.lblreplec);
        lblPlotSize=(TextView)rootView.findViewById(R.id.lblPlotSize);
        lblProduct=(TextView)rootView.findViewById(R.id.lblProduct);

        Trialcodedata = (TextView) rootView.findViewById(R.id.txtTrialCode);
        //txtNoReplecation=(TextView)rootView.findViewById(R.id.txtNoReplecation);
        //txtPlotNo=(TextView)rootView.findViewById(R.id.txtPlot);

        databaseHelper1 = new databaseHelper(this.getContext());
        msclass = new Messageclass(this.getContext());
        cx = this.getContext();

        Btnsave = (Button) rootView.findViewById(R.id.btnSave);
        BtnFieldTag = (Button) rootView.findViewById(R.id.BtnFieldTag);
        BtnUpdate=(Button)rootView.findViewById(R.id.btnUpdate);
        //Get Data
        String getArgument = getArguments().getString("keyCode");
        Trialcodedata.setText(getArgument);
        // String input = Trialcodedata.getText().toString();
        //End
        //Asing to textview

        //   String Area = getArguments().getString("AreaInAcr");
        //  txtArea.setText(Area);

// Get Location
        getLocation();
/// End Location
        Cursor data1 = databaseHelper1.fetchusercode();

        if (data1.getCount() == 0) {
            msclass.showMessage("No Data Available... ");
        } else {
            data1.moveToFirst();
            if (data1 != null) {
                do {
                    userCode = data1.getString((data1.getColumnIndex("user_code")));
                } while (data1.moveToNext());

            }
            data1.close();
        }

        Cursor data = databaseHelper1.fetchData(Trialcodedata.getText().toString());

        if (data.getCount() == 0) {
            msclass.showMessage("No Data Available... ");
        } else {
            data.moveToFirst();
            if (data != null) {
                do {
                    //txtNoReplecation = data.getInt(0);
                    txtReplec = data.getString(data.getColumnIndex("REPLECATION"));
                    //PLOTNO=data.getString(data.getColumnIndex("PLOTSTART"));
                    //txtNoReplecation.setText(data.getColumnIndex("REPLECATION"));
                    NoRows = data.getString(data.getColumnIndex("NoRows"));
                    RowLength = data.getString(data.getColumnIndex("RowLength"));
                    RRSpecing = data.getString(data.getColumnIndex("RRSpecing"));
                    PPSpacing = data.getString(data.getColumnIndex("PPSpacing"));
                    TotEntry = data.getString(data.getColumnIndex("ENTRY"));
                    Location = data.getString(data.getColumnIndex("Location"));
                    plotstart = data.getString(data.getColumnIndex("StartPlotNo"));
                    Plotend = data.getString(data.getColumnIndex("EndPlotNo"));
                    trial_type=data.getString(data.getColumnIndex("Trail_Type"));
                    PlotSize=data.getString(data.getColumnIndex("PlotSize"));
                    nursery=data.getString(data.getColumnIndex("nursery"));
                    product=data.getString((data.getColumnIndex("Product")));

                } while (data.moveToNext());
                txtReplication.setText(txtReplec);
                txtNoRows.setText(NoRows);
                txtRowLength.setText(RowLength);
                txtRRSpecing.setText(RRSpecing);
                txtPPSpacing.setText(PPSpacing);
                txtTotEntry.setText(TotEntry);
                txtLocation.setText(Location);
                txtplotstart.setText(plotstart + "  To  " + Plotend);
                txtPlotSize.setText(PlotSize);
                txtProduct.setText(product);

            }
            data.close();
        }

        if (nursery.equals("N")){txtnurseryDate.setVisibility(View.GONE);
        txtSowingDate.setHint("Select Sowing Date");}else {txtnurseryDate.setVisibility(View.VISIBLE);}

        if (trial_type.equals("DEM")){txtProduct.setVisibility(View.VISIBLE);lbl12.setVisibility(View.VISIBLE);lblProduct.setVisibility(View.VISIBLE);}

        if (trial_type.equals("MET")){
            lblRowLength.setVisibility(View.GONE);
            lblRRSpecing.setVisibility(View.VISIBLE);
            lblPPSpacing.setVisibility(View.VISIBLE);
            lblReplication.setVisibility(View.VISIBLE);
            lblPlotSize.setVisibility(View.GONE);

            lbl4.setVisibility(View.GONE);
            lbl5.setVisibility(View.VISIBLE);
            lbl6.setVisibility(View.VISIBLE);
            lbl7.setVisibility(View.VISIBLE);
            lbl11.setVisibility(View.GONE);

            txtRowLength.setVisibility(View.GONE);//
            txtRRSpecing.setVisibility(View.VISIBLE);//
            txtPPSpacing.setVisibility(View.VISIBLE);//
            txtReplication.setVisibility(View.VISIBLE); //
            txtPlotSize.setVisibility(View.GONE); //
        } else {
            lblRowLength.setVisibility(View.VISIBLE);
            lblRRSpecing.setVisibility(View.GONE);
            lblPPSpacing.setVisibility(View.GONE);
            lblReplication.setVisibility(View.GONE);
            lblPlotSize.setVisibility(View.VISIBLE);


            lbl4.setVisibility(View.VISIBLE);
            lbl5.setVisibility(View.GONE);
            lbl6.setVisibility(View.GONE);
            lbl7.setVisibility(View.GONE);
            lbl11.setVisibility(View.VISIBLE);

            txtRowLength.setVisibility(View.VISIBLE);//
            txtRRSpecing.setVisibility(View.GONE);//
            txtPPSpacing.setVisibility(View.GONE);//
            txtReplication.setVisibility(View.GONE); //
            txtPlotSize.setVisibility(View.VISIBLE); //
        }

        Cursor Farmerdata = databaseHelper1.fetchFarmerData(Trialcodedata.getText().toString());

        if (Farmerdata.getCount() == 0) {
            //msclass.showMessage("No Data Available... ");
        } else {
            Farmerdata.moveToFirst();
            if (Farmerdata != null) {
                do {
                    //Fname ,Fvillage ,Fmobile ,FstartNote ,Fsowingdate
                    //txtNoReplecation = data.getInt(0);
                    Fname = Farmerdata.getString(Farmerdata.getColumnIndex("Fname"));
                    Fvillage = Farmerdata.getString(Farmerdata.getColumnIndex("Fvillage"));
                    Fmobile = Farmerdata.getString(Farmerdata.getColumnIndex("Fmobile"));
                    FstartNote = Farmerdata.getString(Farmerdata.getColumnIndex("FstartNote"));
                    Fsowingdate = Farmerdata.getString(Farmerdata.getColumnIndex("Fsowingdate"));
                    nurseryDate=Farmerdata.getString(Farmerdata.getColumnIndex("nurseryDate"));
                } while (Farmerdata.moveToNext());
                txtFname.setText(Fname);
                txtFvillage.setText(Fvillage);
                txtFmobile.setText(Fmobile);
                txtStartnote.setText(FstartNote);
                txtSowingDate.setText(Fsowingdate);
                txtnurseryDate.setText(nurseryDate);

                Btnsave.setEnabled(false);
                BtnFieldTag.setEnabled(true);

                if(txtnurseryDate.length()>0){
                    BtnUpdate.setVisibility(View.VISIBLE);
                }
            }
            Farmerdata.close();
        }

        //End
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {


            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdformat = new SimpleDateFormat(myFormat, Locale.US);
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //updateLabel();
                // txtSowingDate.setText(dayOfMonth + "/" + monthOfYear+1 + "/" + year);
                txtSowingDate.setText(sdformat.format(myCalendar.getTime()));
               // txtnurseryDate.setText(sdformat.format(myCalendar.getTime()));
                // txtSowingDate.setText(new StringBuilder().append(dayOfMonth).append("-").append(monthOfYear + 1).append("-").append(year).append(""));
            }

        };

        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {


            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdformat = new SimpleDateFormat(myFormat, Locale.US);
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //updateLabel();
                // txtSowingDate.setText(dayOfMonth + "/" + monthOfYear+1 + "/" + year);
                txtnurseryDate.setText(sdformat.format(myCalendar.getTime()));
                // txtSowingDate.setText(new StringBuilder().append(dayOfMonth).append("-").append(monthOfYear + 1).append("-").append(year).append(""));
            }

        };

        txtSowingDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        txtnurseryDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date1, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        Btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation() == true) {
                    boolean result = databaseHelper1.InsertFarmerData(txtFname.getText().toString(), txtFvillage.getText().toString(),
                            txtFmobile.getText().toString(), txtStartnote.getText().toString(), txtSowingDate.getText().toString(),
                            Trialcodedata.getText().toString(), "0", userCode,txtGeoVillage.getText().toString(),txtnurseryDate.getText().toString());
                    if (result) {
                        //databaseHelper1.updatePlot(Integer.parseInt(txtPlotNo.getText().toString()), Trialcodedata.getText().toString());
                        msclass.showMessage("Data Save Sucessfully");
                        Btnsave.setEnabled(false);
                        BtnFieldTag.setEnabled(true);
                        //Intent openIntent = new Intent(getActivity(), newAreaTag.class);
                       // openIntent.putExtra("Trail_code", Trialcodedata.getText().toString());
                      //  startActivity(openIntent);
                    } else {
                        msclass.showMessage("Data Not Save");
                    }
                }
            }
        });

        BtnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchQuery1 = "update FarmerInfodata set Fsowingdate = '"+txtSowingDate.getText().toString()+"' where TRIL_CODE='"+Trialcodedata.getText().toString()+"' ";
                databaseHelper1.runQuery(searchQuery1);
                msclass.showMessage("Data Update Sucessfully");
                BtnFieldTag.setEnabled(true);
            }
        });

        BtnFieldTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Bundle data1 = new Bundle();//create bundle instance
                //  data1.putString("keyCode", Trialcodedata.getText().toString());//put string to pass with a key value
                Intent openIntent = new Intent(getActivity(), newAreaTag.class);
                openIntent.putExtra("Trail_code", Trialcodedata.getText().toString());
                startActivity(openIntent);


            }
        });
        return rootView;
    }

    private boolean validation() {
        boolean flag = true;
        if (txtFname.getText().length() == 0) {
            msclass.showMessage("Please Enter Farmername ");
            return false;

        }
        if (txtFvillage.getText().length() == 0) {
            msclass.showMessage("Please enter Village");
            return false;
        }

        if (txtFmobile.getText().length() == 0 || txtFmobile.getText().length()<10) {
            msclass.showMessage("Please Enter 10 digit Mobile No");
            return false;
        }

        if (txtSowingDate.getText().toString().equals("")) {
            msclass.showMessage("Please Select Sowing Date");
            return false;
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        androidx.appcompat.app.ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle("Sowing Details");
        getLocation();
    }

    void getLocation() {
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            // locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,1000,0,this);
            if (ActivityCompat.checkSelfPermission(this.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        longitude=location.getLongitude();
        latitude=location.getLatitude();
       // getLocation();
        try {
            Geocoder geocoder = new Geocoder(this.getContext(), Locale.getDefault());
            List<android.location.Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            txtGeoVillage.setText(""+addresses.get(0).getAddressLine(0)+"");
        }catch(Exception e)
        {

        }

    }
    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this.getContext(), "Please Enable GPS", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

}
