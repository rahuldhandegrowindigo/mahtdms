package com.newtrail.mahyco.trail;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.newtrail.mahyco.trail.ReportDashboard.ReportDashboard;
import com.newtrail.mahyco.trail.TravelManagement.MyTravel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MenuItemAdapter extends RecyclerView.Adapter<MenuItemAdapter.ViewHolder> {
    private ArrayList<MenuPojo> menuPojosList;
    private Context context;


    public MenuItemAdapter(Context context, ArrayList<MenuPojo> menupojo) {
        this.menuPojosList = menupojo;
        this.context = context;

    }

    @Override
    public MenuItemAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.menu_item_card, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {

        viewHolder.txtmenu_name.setText(menuPojosList.get(position).getMenu_name());
        if (menuPojosList.get(position).getImage_id() != 0) {
            Picasso.get().load(menuPojosList.get(position).getImage_id()).into(viewHolder.imgMenu);
        }


        if (menuPojosList.get(position).getMenu_name().equals("Download")) {

            //For Download Data Button
            viewHolder.Crddownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.isBackNavigationAllowed=true;

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    DownloadData downloadData = new DownloadData();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.FragmentContainer, downloadData, downloadData.getTag()).addToBackStack("fragBack").commit();


                }
            });

            //Download Data Button end


            //For Download Observation Button
        }
        else if (menuPojosList.get(position).getMenu_name().equals("Sowing")) {

            viewHolder.Crddownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    home.isBackNavigationAllowed=true;

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    FieldAreaTag fieldAreaTag = new FieldAreaTag();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.FragmentContainer, fieldAreaTag, fieldAreaTag.getTag()).addToBackStack("fragBack").commit();


                }
            });

            //Field Area Button end

            //For Observation Button
        }
        else if (menuPojosList.get(position).getMenu_name().equals("Select Observation")) {


            viewHolder.Crddownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.isBackNavigationAllowed=true;


                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    new_Download_Observation downloadObservation = new new_Download_Observation();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.FragmentContainer, downloadObservation, downloadObservation.getTag()).addToBackStack("fragBack").commit();


                }
            });
            //Download Observation Button end

            //For Field Area  Button

        } else if (menuPojosList.get(position).getMenu_name().equals("Observation")) {

            viewHolder.Crddownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    home.isBackNavigationAllowed=true;

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    Observation observation1 = new Observation();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.FragmentContainer, observation1, observation1.getTag()).addToBackStack("fragBack").commit();


                }
            });

            //For observation Button end
//
//        //For Upload Data Button
        }
        else if (menuPojosList.get(position).getMenu_name().equals("My Travel")) {



            viewHolder.Crddownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, MyTravel.class);
                    context.startActivity(i);
                }
            });
            //For Reporrt Button


        }else if (menuPojosList.get(position).getMenu_name().equals("Upload")) {

            viewHolder.Crddownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    home.isBackNavigationAllowed=true;

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    UploadData uploadData = new UploadData();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.FragmentContainer, uploadData, uploadData.getTag()).addToBackStack("fragBack").commit();

                }
            });


            //Upload Data Button end


            //Feedback
        }  else if (menuPojosList.get(position).getMenu_name().equals("Report")) {



            viewHolder.Crddownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ReportDashboard.class);
                    // Intent i = new Intent(getActivity(), ReportActivity.class);
                    context.startActivity(i);
                }
            });

            //For Map Button

        }
        //else if (menuPojosList.get(position).getMenu_name().equals("BE-Survey (VOTG)")) {
//
//
//
//            viewHolder.Crddownload.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    home.isBackNavigationAllowed=true;
//
//                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
//                    SurveyFragment surveyFragment = new SurveyFragment();
//                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.FragmentContainer, surveyFragment,
//                            surveyFragment.getTag()).addToBackStack("fragBack").commit();
//                }
//            });
//
//            //For Map Button
//
//        }
            else if (menuPojosList.get(position).getMenu_name().equals("Feedback")) {
            viewHolder.Crddownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    home.isBackNavigationAllowed=true;

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    FeedbackFragment feedbackFragment = new FeedbackFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.FragmentContainer, feedbackFragment,
                            feedbackFragment.getTag()).addToBackStack("fragBack").commit();


                }
            });


            //For Logout Button
        }
        else if (menuPojosList.get(position).getMenu_name().equals("Map")) {
            viewHolder.Crddownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, MapsActivity.class);
                    context.startActivity(i);
                }
            });

            //For Feedback Button

        }else if (menuPojosList.get(position).getMenu_name().equals("Logout")) {

            viewHolder.Crddownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences pref;
                    SharedPreferences.Editor editor;
                    pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
                    editor = pref.edit();
                    editor.clear();
                    editor.commit();
                    Logout logout1 = new Logout();
                    Intent openIntent = new Intent(context, login.class);
                    openIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(openIntent);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return menuPojosList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtmenu_name;
        private ImageView imgMenu;
        private CardView Crddownload;

        public ViewHolder(View view) {
            super(view);

            txtmenu_name = (TextView) view.findViewById(R.id.txtmenu_name);
            imgMenu = (ImageView) view.findViewById(R.id.imgMenu);
            Crddownload = (CardView) view.findViewById(R.id.Crddownload);
        }
    }

}