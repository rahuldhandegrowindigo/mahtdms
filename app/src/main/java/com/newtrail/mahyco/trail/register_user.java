package com.newtrail.mahyco.trail;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
//import com.trial.mahyco.trail.Config;

import java.util.UUID;
import java.util.concurrent.ExecutionException;


public class register_user extends AppCompatActivity {
    private TextView txtView, txtentermobile, txtEnterotp, lbltype;
    private EditText otp;
    private CardView btnLogin, btnOtp;
    String type = null;
    databaseHelper databaseHelper1;
    //public Button btnLogin;
    ProgressDialog dialog;
    public String SERVER = "http://cmr.mahyco.com/BreaderDataHandler.ashx";
    public Messageclass msclass;
    public CommonExecution cx;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Config config;
    String imeiNumber;
    TelephonyManager tel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        getSupportActionBar().setTitle("Register User");
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
         //imeiNumber = tm.getDeviceId();

        Log.d("imei","imei"+imeiNumber);
        btnLogin= (CardView)findViewById(R.id.btnLogin);
        txtentermobile=(TextView)findViewById(R.id.txtentermobile);
        txtEnterotp=(TextView)findViewById(R.id.txtEnterotp);
        msclass=new Messageclass(this);
        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        cx=new CommonExecution(this);
        config = new Config(this); //Here the context is passing
        databaseHelper1=new databaseHelper(this);
        tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        getUUID();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation() == true) {
                   dialog.setMessage("Loading. Please wait...");
                    dialog.show();

                    String searchQuery1 = "delete from UserMaster ";
                    databaseHelper1.runQuery(searchQuery1);
                    UserRegisteration();
                }

            }
        });
    }
    public void getUUID() {


        String uuid = pref.getString("uuid", "");

        String deviceId;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.READ_PHONE_STATE}, 101);
            return;
        }
        if (tel.getDeviceId() != null) {
            deviceId = tel.getDeviceId();
            imeiNumber=deviceId;
            editor.putString("uuid", deviceId);
            editor.apply();

        } else {


            if (uuid.isEmpty()) {
                deviceId = UUID.randomUUID().toString();
                editor.putString("uuid", deviceId);
                editor.commit();
                imeiNumber=deviceId;
            } else {
                imeiNumber=uuid;
            }


        }


    }
    private boolean validation()
    {
        boolean flag=true;
        if(txtentermobile.getText().length()==0)
        {
            msclass.showMessage("Please enter user name ") ;
            return false;

        }
        if(txtEnterotp.getText().length()==0)
        {
            msclass.showMessage("Please  enter password") ;
            return false;
        }

        return true;
    }

    private  boolean UserRegisteration()
    {
        if(config.NetworkConnection()) {
            dialog.setMessage("Loading....");
            dialog.show();
            String str= null;
            boolean fl=false;
            try {
                str = cx.new BreederMasterData(1,txtentermobile.getText().toString(),txtEnterotp.getText().toString(),imeiNumber.trim()).execute().get();

                if(str.contains("False"))
                {
                    msclass.showMessage("Registration Data not available");
                    dialog.dismiss();
                }


                else {
                    // msclass.showMessage(str);
                    JSONObject object = new JSONObject(str);
                    Log.d("","DataWe"+object);
                    JSONArray jArray = object.getJSONArray("Table");
                    Log.d("","DataWe"+jArray);

                    for (int i = 0; i < jArray.length(); i++) {

                        JSONObject jObject = jArray.getJSONObject(0);

                        Log.d("","DataWe"+jObject);
                        if(jObject.getString("IMEI")!=(imeiNumber)) {

                            msclass.showMessage("Already Registered on other device");//show specific response msg from api
                            dialog.dismiss();

                        }
                            databaseHelper1.deleledata("UserMaster", "");
                            fl = databaseHelper1.InsertUserRegistration(jObject.getString("USER_CODE").toString().toUpperCase(), jObject.getString("USER_NAME").toString(), jObject.getString("USER_PWD").toString(), jObject.getString("USER_ROLE").toString(),jObject.getString("IMEI").toString());
                            editor.putString("Displayname", jObject.getString("USER_NAME").toString());

                        editor.putString("USER_IMEINO",jObject.getString("IMEI"));

                        //}
                        //else
                       // {
                        //    msclass.showMessage("This user mobile device IMEI No and User id not match .please check IMEI_no");
                        //    dialog.dismiss();
                        ////    return false;
                        //}


                    }
                    editor.putString("USER_CODE", txtentermobile.getText().toString());
                    editor.commit();

                    if (fl == true) {
                    //    msclass.showMessage("User Registration successfully");
                        dialog.dismiss();
                        txtEnterotp.setText("");
                        txtentermobile.setText("");
                        Intent intent= new Intent(getApplicationContext(),login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        return true;
                        // finish();

                    } else {
                        msclass.showMessage("Registration  not done");
                        return false;
                    }


                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else
        {
            msclass.showMessage("Internet network not available.");
            dialog.dismiss();
            return false;
        }
        return true;
    }
}

