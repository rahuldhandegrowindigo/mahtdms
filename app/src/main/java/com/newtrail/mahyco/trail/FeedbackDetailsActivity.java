package com.newtrail.mahyco.trail;

import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;


import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import androidx.core.content.ContextCompat;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

//import com.ceylonlabs.imageviewpopup.ImagePopup;

public class FeedbackDetailsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public Messageclass msclass;
    public CommonExecution cx;
    Spinner ddlspinnerRank;//28032019
    Config config;
    ProgressDialog dialog;
    public LinearLayout my_linear_layout1;
    public ScrollView container;
    SharedPreferences locdata;
    SharedPreferences.Editor editor, locedit;
    databaseHelper databaseHelper1;
    public String userCode;
    public TextView ratingCount,txtFname, txtTrialCode, txtProduct, db, txtLocation, txtSowing, txtPlotNo, txtENDPlotNo, txtPlotNo1, lblCordinate;
    public Button save, next, back, btnGo;//28032019
    public String last;
    EditText txtEnerPlot, remarks;
    String ratingVal;
    RatingBar ratingbar;
    String Ranking;
    private ImageView ivImage;
    private ImageView img_star1;
    private ImageView img_star2;
    private ImageView img_star3;
    private ImageView img_star4;
    private ImageView img_star5;
   RelativeLayout relcount;
   RelativeLayout relanim;
    TextView ratingCountanim;

    LottieAnimationView lottieAnimationView;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_details);
        getSupportActionBar().setTitle("Feedback");//28032019
        config = new Config(this); //Here the context is passing
        msclass = new Messageclass(this);
        cx = new CommonExecution(this);
        databaseHelper1 = new databaseHelper(this);
        container = (ScrollView) findViewById(R.id.container);
        dialog = new ProgressDialog(this);
        final MediaPlayer MP = MediaPlayer.create(this, R.raw.ting);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        locdata = getApplicationContext().getSharedPreferences("locdata", 0); // 0 - for private mode
        locedit = locdata.edit();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        txtTrialCode = (TextView) findViewById(R.id.txtTrialCode);
        txtFname = (TextView) findViewById(R.id.txtFname);
        txtProduct = (TextView) findViewById(R.id.txtProduct);
        txtLocation = (TextView) findViewById(R.id.txtLocation);
        txtSowing = (TextView) findViewById(R.id.txtSowing);
        txtPlotNo = (TextView) findViewById(R.id.txtPlotNo);
        txtENDPlotNo = (TextView) findViewById(R.id.txtENDPlotNo);
        txtPlotNo1 = (TextView) findViewById(R.id.txtPlotNo1);
        lblCordinate = (TextView) findViewById(R.id.lblCordinate);
        // final ImagePopup imagePopup = new ImagePopup(this);

        save = (Button) findViewById(R.id.btnSaveObs);
        next = (Button) findViewById(R.id.btnNextObs);
        back = (Button) findViewById(R.id.btnBack);//28032019
        btnGo = (Button) findViewById(R.id.btnGo);
        ddlspinnerRank = (Spinner) findViewById(R.id.spinnerRank);//28032019
        remarks = (EditText) findViewById(R.id.remarks);//28032019

        txtEnerPlot = (EditText) findViewById(R.id.txtEnerPlot);
        ratingCount=(TextView)findViewById(R.id.ratingCount);
        relcount=(RelativeLayout)findViewById(R.id.relcount);
        relanim=(RelativeLayout)findViewById(R.id.relanim);
        ratingCountanim = (TextView) findViewById(R.id.ratingCountanim);

        ddlspinnerRank.setOnItemSelectedListener(this);//28032019

        lottieAnimationView = (LottieAnimationView) findViewById(R.id.lottieAnimationView);
//startCheckAnimation();
        ivImage = (ImageView) findViewById(R.id.ivImage);
        img_star1 = (ImageView) findViewById(R.id.img_star1);
        img_star2 = (ImageView) findViewById(R.id.img_star2);
        img_star3 = (ImageView) findViewById(R.id.img_star3);
        img_star4 = (ImageView) findViewById(R.id.img_star4);
        img_star5 = (ImageView) findViewById(R.id.img_star5);
        img_star1.setTag("0");
        img_star2.setTag("0");
        img_star3.setTag("0");
        img_star4.setTag("0");
        img_star5.setTag("0");
        img_star1.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

            @Override
            public void onClick(View v) {
                if (img_star1.getTag().toString().equals("1")) {
                    setUnRate(1);
                } else
                    setRating(1);

            }
        });
        img_star2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (img_star2.getTag().toString().equals("1")) {
                    setUnRate(2);
                } else
                    setRating(2);

            }
        });
        img_star3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (img_star3.getTag().toString().equals("1")) {
                    setUnRate(3);
                } else
                    setRating(3);

            }
        });

        img_star4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (img_star4.getTag().toString().equals("1")) {
                    setUnRate(4);
                } else
                    setRating(4);
            }
        });
        img_star5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (img_star5.getTag().toString().equals("1")) {
                    setUnRate(5);
                } else
                    setRating(5);

            }
        });

        loadSpinnerData();//28032019
        ddlspinnerRank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {//28032019
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {

                    Ranking = ddlspinnerRank.getSelectedItem().toString();
                    Toast.makeText(getApplicationContext(), Ranking, Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


//        addListenerOnButtonClick();

        Intent i = getIntent();
        String name = i.getStringExtra("trailCode");
        txtTrialCode.setText(name);
        String crop = null;
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }

        final Cursor data = databaseHelper1.fetchusercode();

        if (data.getCount() == 0) {
            msclass.showMessage("No Data Available... ");
        } else {
            data.moveToFirst();
            if (data != null) {
                do {
                    userCode = data.getString((data.getColumnIndex("user_code")));
                } while (data.moveToNext());

            }
            data.close();
        }
        try {

            Cursor Farmerdata1 = databaseHelper1.ForTakenobservation(txtTrialCode.getText().toString());
            if (Farmerdata1.getCount() == 0) {
                last = "";

            } else {
                Farmerdata1.moveToFirst();
                if (Farmerdata1 != null) {
                    do {
                        last = Farmerdata1.getString((Farmerdata1.getColumnIndex("PlotNo")));

                    } while (Farmerdata1.moveToNext());
                }
                Farmerdata1.close();
            }


            Cursor Farmerdata = databaseHelper1.Forobservation(txtTrialCode.getText().toString());
            if (Farmerdata.getCount() == 0) {
            } else {
                Farmerdata.moveToFirst();
                if (Farmerdata != null) {
                    do {
                        txtFname.setText(Farmerdata.getString(Farmerdata.getColumnIndex("Fname")));
                        txtProduct.setText(Farmerdata.getString(Farmerdata.getColumnIndex("Product")));
                        txtLocation.setText(Farmerdata.getString(Farmerdata.getColumnIndex("Fvillage")));
                        txtSowing.setText(Farmerdata.getString(Farmerdata.getColumnIndex("Fsowingdate")));
                        txtPlotNo1.setText(Farmerdata.getString(Farmerdata.getColumnIndex("StartPlotNo")));

                        if (last.equals("")) {
                            txtPlotNo.setText(Farmerdata.getString(Farmerdata.getColumnIndex("StartPlotNo")));
                        } else {
                            txtPlotNo.setText(last);
                        }
                        crop = Farmerdata.getString((Farmerdata.getColumnIndex("CROP")));
                        txtENDPlotNo.setText(Farmerdata.getString((Farmerdata.getColumnIndex("EndPlotNo"))));


                        ////Alert box


                        AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackDetailsActivity.this);

                        builder.setMessage("Farmer Name : " + "" + Farmerdata.getString(Farmerdata.getColumnIndex("Fname")) +
                                "\n\nVillage : " + "" + Farmerdata.getString(Farmerdata.getColumnIndex("Fvillage")) + "" +
                                "\n\nSowing Date : " + "" + Farmerdata.getString(Farmerdata.getColumnIndex("Fsowingdate")));

                        //  msclass.showMessage("Value Must Be In Between"+" '"+min+"' " +"and"+" '"+max+"'");
                        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing, but close the dialog
                                dialog.dismiss();
                            }
                        });

                        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();

                        ////Alert box end


                    } while (Farmerdata.moveToNext());
                }
                Farmerdata.close();
            }
            //// Data TO Show On Screen End
            if (txtPlotNo.equals(txtENDPlotNo.getText().toString())) {
                msclass.showMessage("All Plot Observation Are taken ");
            }
            final String Tcode = txtTrialCode.getText().toString();

            //////------------------------------28032019-----------------------------//////

            getValues();
            /////////---------------------------end---------------------------------////////


//////------------------------------28032019-----------------------------//////

            btnGo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {//28032019


                    String s = txtEnerPlot.getText().toString();
                    if (s != null && !s.isEmpty()) {

                        String GoPlotNo = txtEnerPlot.getText().toString().trim();

                        int UpdatePlot;
                        UpdatePlot = Integer.parseInt(GoPlotNo.toString());

                        String plot1 = txtPlotNo1.getText().toString();
                        Log.d("testimonial", "onClick: " + plot1);
                        if (UpdatePlot < Integer.parseInt(txtPlotNo1.getText().toString())) {
                            msclass.showMessage("This Plot No Is Not Available. \nPlease Check Plot No You Enter");
                        } else if (UpdatePlot > Integer.parseInt(txtENDPlotNo.getText().toString())) {
                            msclass.showMessage("This Plot No Is Not Available.  \nPlease Check Plot No You Enter");
                        } else {
                            txtPlotNo.setText(String.valueOf(UpdatePlot));//28032019
                            txtEnerPlot.setText("");//28032019

                            //   BindData(finalCrop, false);
                        }


                    }

                    getValues();

                }
            });


//////------------------------------28032019-----------------------------//////
            save.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {//28032019

                    //setRatingVal();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    final String entrydate = sdf.format(new Date());
                    int feedbackNumber = (int) databaseHelper1.getFeedbackCount(entrydate, txtTrialCode.getText().toString(), txtPlotNo.getText().toString());
                    Log.d("feedback number", "feedback" + feedbackNumber);

                    databaseHelper1.close();

                    String Is_Synced = "0";
                    String Is_Submitted = "0";




                        if (feedbackNumber == 0) {

                            databaseHelper1.insertFeedback(entrydate.toString(), txtTrialCode.getText().toString(), txtPlotNo.getText().toString(), Ranking.toString(), Is_Synced.toString(), ratingVal.toString(), remarks.getText().toString(), Is_Submitted.toString());


                        } else {

                            databaseHelper1.updateFeedback(entrydate.toString(), txtTrialCode.getText().toString(), txtPlotNo.getText().toString(), Ranking.toString(), Is_Synced.toString(), ratingVal.toString(), remarks.getText().toString(), Is_Submitted.toString());


                        }
                        try {
                            int UpdatePlot;
                            UpdatePlot = Integer.parseInt(txtPlotNo.getText().toString()) + Integer.parseInt("1");
                            if (UpdatePlot > Integer.parseInt(txtENDPlotNo.getText().toString())) {

                                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                databaseHelper1.updateFeedbackAtLast(entrydate.toString(),
                                                        txtTrialCode.getText().toString(), "1");
                                                FeedbackDetailsActivity.super.onBackPressed();
                                                break;

                                            case DialogInterface.BUTTON_NEGATIVE:

                                                alert2();
                                                break;
                                        }
                                    }
                                };

                                AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackDetailsActivity.this);
                                builder.setTitle("Feedback");
                                builder.setMessage("This is the last plot, are you sure to submit data? Yes for submit and No for deleting the record").setPositiveButton("Yes", dialogClickListener)
                                        .setNegativeButton("No", dialogClickListener).show();

                            } else {
                                txtPlotNo.setText(String.valueOf(UpdatePlot));
                            }
                            getValues();
                            MP.start();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                }

            });


            txtPlotNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(FeedbackDetailsActivity.this, AndroidDatabaseManager.class);

                    startActivity(intent);
                }
            });

            back.setOnClickListener(new View.OnClickListener() {//28032019
                @Override
                public void onClick(View v) {//28032019

                    int UpdatePlot;
                    UpdatePlot = Integer.parseInt(txtPlotNo.getText().toString()) - Integer.parseInt("1");

                    if (UpdatePlot < Integer.parseInt(txtPlotNo1.getText().toString())) {
                        msclass.showMessage("This is Starting Plot You Cant Go Previous ");
                    } else {
                        txtPlotNo.setText(String.valueOf(UpdatePlot));

                    }
                    getValues();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

        }


    }

    private Boolean validationSuccess() {//28032019
        if (remarks.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Please enter remarks", Toast.LENGTH_SHORT).show();
            return false;
        }


//        if(ddlspinnerRank.getSelectedItemPosition()==0){
//            Toast.makeText(getApplicationContext(),"Please select ranking",Toast.LENGTH_SHORT).show();
//            return false;
//        }
        return true;
    }

    private void loadSpinnerData() {//28032019


        List<String> ddRankingList = Arrays.asList(getResources().getStringArray(R.array.Ranking));
        ArrayAdapter<String> ddYearlistAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                ddRankingList);
        ddYearlistAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        ddlspinnerRank.setAdapter(ddYearlistAdapter);

    }


//    public void addListenerOnButtonClick(){
//        ratingbar=(RatingBar)findViewById(R.id.ratingBar);
//        //ratingbar.setNumStars(5);
//        //ratingbar.setMax(5);
//        ratingbar.setStepSize(1.0f);
//        //Performing action on Button Click
//        ratingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
//            @Override
//            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
//
// ratingVal= String.format("%.0f", rating);
//                Toast.makeText(getApplicationContext(), ratingVal, Toast.LENGTH_LONG).show();
//            }
//        });
//
//    }

    @Override
    public void onBackPressed() {
        //28032019

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        final String entrydate = sdf.format(new Date());
        int feedbackNumber = (int) databaseHelper1.getFeedbackCountBack(entrydate.toString(), txtTrialCode.getText().toString(), "0");

        databaseHelper1.close();
        if (feedbackNumber > 0) {
            alert1();
        } else {


            this.finish();

        }

    }

    //////------------------------------28032019-----------------------------//////
    public void getValues() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        final String entrydate = sdf.format(new Date());
        final Cursor FeedbackData = databaseHelper1.getFeedbackData(txtPlotNo.getText().toString(), txtTrialCode.getText().toString(), entrydate.toString());

        Log.d("curserFeedback data", FeedbackData.toString());
        ratingVal="0";
        if (FeedbackData.getCount() == 0) {

            remarks.setText("");
            setUnRate(1);
            ddlspinnerRank.setSelection(0);


        } else {
            FeedbackData.moveToFirst();
            if (FeedbackData != null) {
                do {
                    remarks.setText(FeedbackData.getString(FeedbackData.getColumnIndex("Remarks")));
                    count = setRating((FeedbackData.getInt(FeedbackData.getColumnIndex("Rating"))));
                    ddlspinnerRank.setSelection(Integer.parseInt(FeedbackData.getString(FeedbackData.getColumnIndex("Ranking"))));

                }
                while (FeedbackData.moveToNext());

            }
            FeedbackData.close();

        }
    }

//////------------------------------28032019-----------------------------//////

    public void alert1() {


        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        final String entrydate = sdf.format(new Date());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Are You Sure You want to go back?");
        builder.setMessage("Your data will be lost.");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                databaseHelper1.deleledata("FeedbackTaken", " WHERE " + "TRIAL_CODE='" + txtTrialCode.getText().toString() + "' AND " + "DATE='"
                        + entrydate.toString() + "' AND " + "isSubmitted='" + "0" + "' AND " + "isSyncedStatus='" + "0" + "'");

                FeedbackDetailsActivity.super.onBackPressed();


            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


                if (Integer.parseInt(txtPlotNo.getText().toString()) < Integer.parseInt(txtENDPlotNo.getText().toString())) {
                    txtPlotNo.setText(txtENDPlotNo.getText().toString());

                    msclass.showMessage("This is Last Plot");

                }
            }
        });
        builder.show();


    }

    //////------------------------------28032019-----------------------------//////
    public void alert2() {


        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        final String entrydate = sdf.format(new Date());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Are You Sure You want to go back?");
        builder.setMessage("Your data will be lost.");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                databaseHelper1.deleledata("FeedbackTaken", " WHERE " + "TRIAL_CODE='" + txtTrialCode.getText().toString() + "' AND " + "DATE='"
                        + entrydate.toString() + "' AND " + "isSubmitted='" + "0" + "' AND " + "isSyncedStatus='" + "0" + "'");

                FeedbackDetailsActivity.super.onBackPressed();


            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


                if (Integer.parseInt(txtPlotNo.getText().toString()) < Integer.parseInt(txtENDPlotNo.getText().toString())) {
                    txtPlotNo.setText(txtENDPlotNo.getText().toString());


                    FeedbackDetailsActivity.super.onBackPressed();
                }
            }
        });
        builder.show();


    }


    public int setRating(int star) {
        relanim.setVisibility(View.GONE);
        relcount.setVisibility(View.VISIBLE);
        if (star == 0) {
            img_star1.setTag("0");
            img_star2.setTag("0");
            img_star3.setTag("0");
            img_star4.setTag("0");
            img_star5.setTag("0");
            img_star1.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));

            img_star2.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            img_star3.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            img_star4.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            img_star5.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            ratingVal="0";
            relanim.setVisibility(View.GONE);
            relcount.setVisibility(View.GONE);

        } else if (star == 1) {
            img_star1.setTag("1");

            img_star1.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));
            ratingVal="1";
            ratingCount.setText(ratingVal);
            relcount.setVisibility(View.VISIBLE);

        } else if (star == 2) {
            img_star1.setTag("1");
            img_star2.setTag("1");
            img_star1.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));
            img_star2.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));

            ratingVal="2";
            ratingCount.setText(ratingVal);
            relcount.setVisibility(View.VISIBLE);
        } else if (star == 3) {
            img_star1.setTag("1");
            img_star2.setTag("1");
            img_star3.setTag("1");
            img_star1.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));
            img_star2.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));
            img_star3.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));

            ratingVal="3";
            ratingCount.setText(ratingVal);
            relcount.setVisibility(View.VISIBLE);
        } else if (star == 4) {
            img_star1.setTag("1");
            img_star2.setTag("1");
            img_star3.setTag("1");
            img_star4.setTag("1");

            img_star1.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));
            img_star2.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));
            img_star3.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));
            img_star4.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));

            ratingVal="4";
            ratingCount.setText(ratingVal);
            relcount.setVisibility(View.VISIBLE);
        } else if (star == 5) {
            img_star1.setTag("1");
            img_star2.setTag("1");
            img_star3.setTag("1");
            img_star4.setTag("1");
            img_star5.setTag("1");
            img_star1.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));
            img_star2.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));
            img_star3.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));
            img_star4.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));
            img_star5.setImageDrawable(getResources().getDrawable(R.drawable.star_filled));
            ratingVal="5";
            relcount.setVisibility(View.GONE);

            ratingCountanim.setText(ratingVal);
            relanim.setVisibility(View.VISIBLE);
            startCheckAnimation();

        }
        Log.d("rohitt", "setRating1: " + count);
        return star;

    }

    public void setUnRate(int star) {
        if (star == 1) {

            img_star1.setTag("0");
            img_star2.setTag("0");
            img_star3.setTag("0");
            img_star4.setTag("0");
            img_star5.setTag("0");
            img_star1.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            img_star2.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            img_star3.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            img_star4.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            img_star5.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            ratingVal="0";
            relanim.setVisibility(View.GONE);
            relcount.setVisibility(View.GONE);

        } else if (star == 2) {
            img_star2.setTag("0");
            img_star3.setTag("0");
            img_star4.setTag("0");
            img_star5.setTag("0");

            img_star2.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            img_star3.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            img_star4.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));

            img_star5.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            ratingVal="1";
            ratingCount.setText(ratingVal);
            relanim.setVisibility(View.GONE);
            relcount.setVisibility(View.VISIBLE);


        } else if (star == 3) {
            img_star3.setTag("0");
            img_star4.setTag("0");
            img_star5.setTag("0");
            img_star3.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            img_star4.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            img_star5.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            ratingVal="2";
            ratingCount.setText(ratingVal);
            relanim.setVisibility(View.GONE);
            relcount.setVisibility(View.VISIBLE);


        } else if (star == 4) {
            img_star4.setTag("0");
            img_star5.setTag("0");
            img_star4.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));

            img_star5.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            ratingVal="3";
            ratingCount.setText(ratingVal);
            relanim.setVisibility(View.GONE);
            relcount.setVisibility(View.VISIBLE);


        } else if (star == 5) {
            img_star5.setTag("0");

            img_star5.setImageDrawable(getResources().getDrawable(R.drawable.star_empty));
            ratingVal="4";
            ratingCount.setText(ratingVal);
            relanim.setVisibility(View.GONE);
            relcount.setVisibility(View.VISIBLE);

        }
      //  count = star;
      //  ratingVal = String.valueOf(count);
        Log.d("rohitt", "setRating2: " + count);

    }

//    private String setRatingVal(){
//
//        int rating = 0;
//
//        if (img_star1.getTag().toString().equals("1")) {
//            rating=  setRating(1);
//        }
//        if (img_star2.getTag().toString().equals("2")){
//
//            rating=setRating(2);
//
//
//        }
//        if (img_star3.getTag().toString().equals("3")){
//
//            rating=setRating(3);
//
//
//        }
//        if (img_star4.getTag().toString().equals("4")){
//
//            rating=setRating(4);
//
//
//        }
//        if (img_star5.getTag().toString().equals("5")){
//
//            rating=setRating(5);
//
//
//        }
//        ratingVal=String.valueOf(rating);
//        return ratingVal;
//    }


    private void startCheckAnimation() {
        ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f).setDuration(1000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                lottieAnimationView.setProgress((Float) valueAnimator.getAnimatedValue());
            }

        });
        animator.start();
        animator.setRepeatCount(3);
// if (lottieAnimationView.getProgress() == 0f) {
//
// } else {
// lottieAnimationView.setProgress(0f);
// lottieAnimationView.setVisibility(View.GONE);
//
// }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}