package com.newtrail.mahyco.trail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
/// Screen to show Sowing and Tagging Details
public class trailDetails_ListAdaptor extends RecyclerView.Adapter<trailDetails_ListAdaptor.DataObjectHolder> {
    private List<TrailReportModel> objects = new ArrayList<TrailReportModel>();

    private Context context;
    private LayoutInflater layoutInflater;
    int imageResourceId = 0;
    FragmentManager fragmentManager;
    public trailDetails_ListAdaptor(List<TrailReportModel> getlist, Context context ,FragmentManager fragmentManager) {
        this.context = context;
        this.objects = getlist;
        this.layoutInflater = LayoutInflater.from(context);
        this.fragmentManager=fragmentManager;
    }
    @NonNull
    @Override
    public trailDetails_ListAdaptor.DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trail_details, parent, false);

        return new trailDetails_ListAdaptor.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull trailDetails_ListAdaptor.DataObjectHolder holder, final int position) {

        holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.Whitecl));

        holder.txt_LocationDetails.setText(objects.get(position).getLocation());
        holder.txt_TrialCodedetails.setText(objects.get(position).getTrailcode());
        holder.txt_TrialTagDetails.setText(objects.get(position).getTagged());
        holder.txt_TrialSegmentDetails.setText(objects.get(position).getTxt_TrialSegmentDetails());

        if(holder.txt_TrialTagDetails.getText().equals("T"))
        {
             holder.btnPLD.setVisibility(View.GONE);
        }

        holder.btnPLD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, pld_not_sown.class);
                i.putExtra("Trail_code", objects.get(position).getTrailcode());
                context.startActivity(i);
            }
        });
        holder.rel_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String taggeed =objects.get(position).getTagged();

                if(taggeed==null)
                {
                    Activity activity = (Activity) context;
                    Bundle data1 = new Bundle();//create bundle instance
                    data1.putString("keyCode", objects.get(position).getTrailcode());//put string to pass with a key value

                    Fragment mFragment = new sowing();
                    mFragment.setArguments(data1);
                    fragmentManager.beginTransaction().replace(R.id.FragmentContainer, mFragment).commit();
                }
                else {
                    Intent i = new Intent(context, alreadyTag.class);
                    i.putExtra("Trail_code", objects.get(position).getTrailcode());
                    context.startActivity(i);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return objects.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private LinearLayout rel_top;
        private RelativeLayout rel_booked_by;
        private CardView cardView;
        private LinearLayout linOrders;
        private TextView txt_LocationDetails;
        private TextView txt_TrialCodedetails;
        private TextView txt_TrialTagDetails;
        private TextView txt_TrialSegmentDetails;
        private Button btnPLD;
        private TextView noorder;
        private RecyclerView recycler;
        ImageView arrow;

        public DataObjectHolder(View view) {
            super(view);
            rel_top = (LinearLayout) view.findViewById(R.id.rel_top);
            cardView = (CardView) view.findViewById(R.id.card_view);
            txt_TrialCodedetails = (TextView) view.findViewById(R.id.txt_TrialCodedetails);
            txt_LocationDetails = (TextView) view.findViewById(R.id.txt_LocationDetails);
            txt_TrialTagDetails= (TextView) view.findViewById(R.id.txt_TrialTagDetails);
            txt_TrialSegmentDetails=(TextView) view.findViewById(R.id.txt_TrialSegmentDetails);
            btnPLD=(Button) view.findViewById(R.id.btnPLD);
            //txt_Location= (TextView) view.findViewById(R.id.txt_Location);


        }
    }
}
