package com.newtrail.mahyco.trail;

public class TrailReportModel {
    TrailReportModel() {

    }

    public String getTxt_TrialSegmentDetails() {
        return txt_TrialSegmentDetails;
    }

    public void setTxt_TrialSegmentDetails(String txt_TrialSegmentDetails) {
        this.txt_TrialSegmentDetails = txt_TrialSegmentDetails;
    }

    private String txt_TrialSegmentDetails;
    private String Trailcode;
    private String Location;

    public String getTrailcode() {
        return Trailcode;
    }

    public void setTrailcode(String trailcode) {
        Trailcode = trailcode;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getTagged() {
        return Tagged;
    }

    public void setTagged(String tagged) {
        Tagged = tagged;
    }

    private String Tagged;
}
