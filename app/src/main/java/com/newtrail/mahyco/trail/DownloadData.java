package com.newtrail.mahyco.trail;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;


import androidx.fragment.app.FragmentTransaction;

import android.widget.TextView;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;

import android.widget.Button;

import com.newtrail.mahyco.trail.utils.MultiSelectionSpinner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
//import com.trial.mahyco.trail.Config;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DownloadData extends Fragment implements MultiSelectionSpinner.OnMultipleItemsSelectedListener {


    public DownloadData() {
        // Required empty public constructor
    }

    String type = null;
    databaseHelper databaseHelper1;
    public Button btnDownload;
    public TextView txtStaff, txtTFA;
    ProgressDialog mprogresss;
    public String SERVER = "http://cmr.mahyco.com/BreaderDataHandler.ashx";
    public Messageclass msclass;
    public CommonExecution cx;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Config config;
    MultiSelectionSpinner ddlSesion;
    MultiSelectionSpinner ddlYear;
    private Toolbar toolbar;
    String userCode;
    RelativeLayout rel_data;
    //    public ProgressBar da;
    String year;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        //Declear veriable from here Rahul Dhande
        msclass = new Messageclass(this.getContext());
        // dialog = new ProgressDialog(getActivity());         //dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //  dialog.setMessage("Loading. Please wait...");
        cx = new CommonExecution(this.getContext());
        config = new Config(this.getContext()); //Here the context is passing
        databaseHelper1 = new databaseHelper(this.getContext());
        // pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        //  editor = pref.edit(this.getContext());

        ////Declear veriable End here Rahul Dhande

        View rootView = inflater.inflate(R.layout.fragment_download_data, container, false);


        ddlYear = (MultiSelectionSpinner) rootView.findViewById(R.id.ddlYear);
        rel_data = (RelativeLayout) rootView.findViewById(R.id.rel_data);
        ddlSesion = (MultiSelectionSpinner) rootView.findViewById(R.id.ddlSesion);
        Spinner TrailStage = (Spinner) rootView.findViewById(R.id.ddlStage);
        Spinner TrailCode = (Spinner) rootView.findViewById(R.id.ddlTrailCode);
        btnDownload = (Button) rootView.findViewById(R.id.btnDownloadData);
        txtStaff = (TextView) rootView.findViewById(R.id.txtStaff);
        txtTFA = (TextView) rootView.findViewById(R.id.txtTFA);
        List<String> ddlTrailType = Arrays.asList(getResources().getStringArray(R.array.TrailTypelist));
        List<String> ddlTraillist = Arrays.asList(getResources().getStringArray(R.array.Traillist));
        //trail type Bind With Spinner
        //ArrayAdapter<String> ddlTrailType = new ArrayAdapter<String>(getActivity(),
        // android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.TrailTypelist));
        //ddlTrailType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ddlYear.setItems(ddlTrailType);
        ddlYear.setListener(this);
        ddlSesion.setItems(ddlTraillist);
        ddlSesion.setListener(this);
        //End

        //Crop Bind With Spinner
        ArrayAdapter<String> ddlCroptype = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.Crop));
        ddlCroptype.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        ddlSesion.setAdapter(ddlCroptype);
//        mprogresss = (ProgressDialog) rootView.findViewById(R.id.progressBar2);
        mprogresss = new ProgressDialog(getActivity());
        // mprogresss.setVisibility(View.INVISIBLE);

        Cursor data = databaseHelper1.fetchusercode();

        if (data.getCount() == 0) {

        } else {
            data.moveToFirst();
            if (data != null) {
                do {
                    userCode = data.getString((data.getColumnIndex("user_code")));
                } while (data.moveToNext());

            }
            data.close();
            txtStaff.setText(userCode);
            // txtStaff.setEnabled(false);
        }


        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {

                    mprogresss.setMessage("Retrieving Data ");
                    mprogresss.show();


//                    mprogresss.setOnTouchListener(new View.OnTouchListener() {
//                        @Override
//                        public boolean onTouch(View v, MotionEvent event) {
//                            return true;
//                        }
//                    });
                    // getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                    // rel_data.setEnabled(false);
                    //  rel_data.setClickable(false);
                   /* new Thread(new Runnable() {
                        @Override
                        public void run() {
                            dowork();
                           // startapp();

                            ;
                        }
                    }).start();*/
                    //DownloadData();
                    try {
                        new BreederDataDownload(2, ddlYear.getSelectedItemsAsString().replace(" ", ""),
                                ddlSesion.getSelectedItemsAsString().replace(" ", ""), txtStaff.getText().toString(),
                                txtTFA.getText().toString()).execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }// else Toast.makeText(getActivity(), "False", Toast.LENGTH_SHORT).show();

            }
        });
        //     getActivity().setProgressBarIndeterminateVisibility(true);
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                dowork();

            }
        }).start();*/

        isUploaded();
        return rootView;

    }

    private void dowork() {
//mprogresss.showContextMenu();
//mprogresss.setIndeterminate(true);
        for (int progress = 0; progress < 100; progress += 10) {
            try {
                Thread.sleep(300);
                //
                //
                // mprogresss.setProgress(progress);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onResume() {
        super.onResume();
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        androidx.appcompat.app.ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle("Download Data");
    }

    private Context getApplicationContext() {
        return null;
    }

    //Start Functions and other code from here

    private boolean validation() {
        boolean flag = true;
        //  Log.d("rohitt", "validation: " + ddlYear.getSelectedItemsAsString());
        if (ddlYear.getSelectedItemsAsString().equals("")) {
            msclass.showMessage("Please Select Year ");
            return false;

        }
        if (ddlSesion.getSelectedItemsAsString().equals("")) {
            msclass.showMessage("Please Select Season");
            return false;
        }
        if (!isUploaded()) {



            final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            alertDialog.setTitle("Mahyco TDMS");
            alertDialog.setMessage("Please Upload Data first" );
            alertDialog.setCanceledOnTouchOutside(false);

            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {



                    home.isBackNavigationAllowed=true;

                    UploadData mainMenu = new UploadData();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.FragmentContainer, mainMenu).addToBackStack("fragBack");
                    fragmentTransaction.commit();


                }
            });

            alertDialog.show();
            return false;
        }

        return flag;
    }


    private boolean DownloadData(String str) {
        boolean fl = false;
        try {
            //  str = cx.new BreederDataDownload(2, ddlYear.getSelectedItem().toString(), ddlSesion.getSelectedItem().toString(), txtStaff.getText().toString(), txtTFA.getText().toString()).execute().get();

            if (str.contains("false")) {
                msclass.showMessage("Data not available");
                // mprogresss.setVisibility(View.INVISIBLE);
            } else {

                // mprogresss.setVisibility(View.VISIBLE);
                databaseHelper1.deleledata("TrailCodeData", "");
                databaseHelper1.deleledata1("ObservationMaster", "");
                databaseHelper1.deleledata1("tagdata1", "");
                databaseHelper1.deleledata1("FarmerInfodata", "");
                databaseHelper1.deleledata1("DownloadedObservation", "");
                databaseHelper1.deleledata1("Observationtaken", "");
                JSONObject object = new JSONObject(str);
                JSONArray jArray = object.getJSONArray("Table"); // For Trial code Download

                JSONArray jArray1 = object.getJSONArray("Table1"); //For Observation Download
                JSONArray jArray2 = object.getJSONArray("Table2"); //For TagData Download
                JSONArray jArray3 = object.getJSONArray("Table3"); //For Sowing Data Download
                JSONArray jArray4 = object.getJSONArray("Table4"); //For Fill Observation Data Download
                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject jObject = jArray.getJSONObject(i);
                    //if(jObject.getString("IMEI").toString().equals(msclass.getDeviceIMEI())) {

                    fl = databaseHelper1.InsertDownloadData(jObject.getString("T_YEAR").toString(), jObject.getString("T_SESION").toString(), jObject.getString("TRIL_CODE").toString(), jObject.getString("Trail_Type").toString(), jObject.getString("CROP").toString(), jObject.getString("ZONE").toString(), jObject.getString("STATE").toString(), jObject.getString("DISTRICT").toString(), jObject.getString("TEHSIL").toString(), jObject.getString("ENTRY").toString(), jObject.getString("REPLECATION").toString(), jObject.getString("STAFF_CODE").toString(), jObject.getString("NoRows").toString(), jObject.getString("RowLength").toString(), jObject.getString("RRSpecing").toString(), jObject.getString("PPSpacing").toString(), jObject.getString("StartPlotNo").toString(), jObject.getString("EndPlotNo").toString(), jObject.getString("Location").toString(), jObject.getString("PlotSize").toString(), jObject.getString("nursery").toString(), jObject.getString("segment").toString(), jObject.getString("Product").toString());


                }

                for (int i = 0; i < jArray1.length(); i++) {

                    JSONObject jObject = jArray1.getJSONObject(i);
                    //if(jObject.getString("IMEI").toString().equals(msclass.getDeviceIMEI())) {

                    Log.d("rohitt", "DownloadObservation respone: " + jObject);


                    fl = databaseHelper1.InsertObservation(jObject.getString("Crop").toString(), jObject.getString("VariableID").toString(), jObject.getString("Name").toString(), jObject.getString("S_M").toString(), jObject.getString("Discription").toString(), jObject.getString("Abbreviation").toString(), jObject.getString("O_Group").toString(), jObject.getString("Variable_type").toString(), jObject.getString("Scale").toString(), jObject.getString("Scale_type").toString(), jObject.getString("Value1").toString(), jObject.getString("Value2").toString(), jObject.getString("Value3").toString(), jObject.getString("Value4").toString(), jObject.getString("Value5").toString(),
                            jObject.getString("Crop_Stage").toString(), jObject.getString("OYT").toString(), jObject.getString("ST").toString(), jObject.getString("MLT").toString(), jObject.getString("PET").toString(), jObject.getString("Demo").toString());


                }

                for (int i = 0; i < jArray2.length(); i++) {

                    JSONObject jObject = jArray2.getJSONObject(i);
                    //if(jObject.getString("IMEI").toString().equals(msclass.getDeviceIMEI())) {

                    fl = databaseHelper1.DownladTagdata(jObject.getString("usercode").toString(), jObject.getString("TRIL_CODE").toString(), jObject.getString("coordinates").toString(), jObject.getString("address").toString(), jObject.getString("entrydate").toString(), jObject.getString("flag").toString());

                }

                for (int i = 0; i < jArray3.length(); i++) {

                    JSONObject jObject = jArray3.getJSONObject(i);
                    //if(jObject.getString("IMEI").toString().equals(msclass.getDeviceIMEI())) {

                    fl = databaseHelper1.DownladFarmerInfodata(jObject.getString("FarmerName").toString(), jObject.getString("FVillage").toString(), jObject.getString("Fmobile").toString(), jObject.getString("FstartNote").toString(), jObject.getString("FSowingDate").toString(), jObject.getString("Trialcode").toString(), jObject.getString("FArea").toString(), jObject.getString("Flag").toString(), jObject.getString("usercode").toString(), jObject.getString("GeoLocation").toString(), jObject.getString("nurseryDate").toString());

                }

                for (int i = 0; i < jArray4.length(); i++) {

                    JSONObject jObject = jArray4.getJSONObject(i);
                    //if(jObject.getString("IMEI").toString().equals(msclass.getDeviceIMEI())) {

                    fl = databaseHelper1.DownloadFillObservation(jObject.getString("VariableID").toString(), jObject.getString("TRIAL_CODE").toString(), jObject.getString("PlotNo").toString(), jObject.getString("Value1").toString(), jObject.getString("Value2").toString(), jObject.getString("Value3").toString(), jObject.getString("Value4").toString(), jObject.getString("Value5").toString(), jObject.getString("usercode"));

                }


            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fl;
    }

//Check for Data uploaded or not
    public boolean isUploaded() {

        String userRole = "";
        Cursor data = databaseHelper1.fetchusercode();

        if (data.getCount() == 0) {
            userRole = "0";


        } else {
            data.moveToFirst();
            if (data != null) {
                do {
                    userRole = data.getString((data.getColumnIndex("USER_ROLE")));


                    Log.d("Role", "RoleMainMenu" + userRole);
                } while (data.moveToNext());

            }
            data.close();


        }


        String searchQueryFarmerInfodata = "select  *  from FarmerInfodata inner join tagdata1 on FarmerInfodata.TRIL_CODE=tagdata1.TRIL_CODE where tagdata1.flag='T' and tagdata1.Upload='U'";
        Cursor cursorFarmerInfodata = databaseHelper1.getReadableDatabase().rawQuery(searchQueryFarmerInfodata, null);
        int count = cursorFarmerInfodata.getCount();
        cursorFarmerInfodata.close();

        String searchQueryObservationtaken = "select  distinct PlotNo  from Observationtaken where flag='0'";
        Cursor cursorObservationtaken = databaseHelper1.getReadableDatabase().rawQuery(searchQueryObservationtaken, null);
        int count1 = cursorObservationtaken.getCount();
        cursorObservationtaken.close();


        String searchQueryPLDNotSown = "select * from PLDNotSown where rowSyncStatus='0'";
        Cursor cursorPLDNotSown = databaseHelper1.getReadableDatabase().rawQuery(searchQueryPLDNotSown, null);
        int count2 = cursorPLDNotSown.getCount();
        cursorPLDNotSown.close();


        String searchQueryFeedbackTaken = "select * from FeedbackTaken where isSyncedStatus='0' and isSubmitted='1' ";
        Cursor cursorFeedbackTaken = databaseHelper1.getReadableDatabase().rawQuery(searchQueryFeedbackTaken, null);
        int count3;
        if (userRole.equals("8")) {
            count3 = cursorFeedbackTaken.getCount();
        } else {

            count3 = 0;

        }
        cursorFeedbackTaken.close();


        Log.d("Count digits", "Count digits" + count + count1 + count2 + count3);

        if (count > 0) {
            return false;
        }

        if (count1 > 0) {
            return false;


        }
        if (count2 > 0) {

            return false;

        }
        if (count3 > 0) {

            return false;

        }


        return true;

    }


    @Override
    public void selectedIndices(List<Integer> indices) {
        Log.d("rohitt", "selectedIndices: " + indices);
    }

    @Override
    public void selectedStrings(List<String> strings) {


        String string = strings.toString();
        string = string.replace(" ", "");
        string = string.replace("[", "").replace("]", "");
        Log.d("rohitt1", "selectedIndices: " + string);

    }


    public class BreederDataDownload extends AsyncTask<String, String, String> {

        private int action;
        private String ddlyear;
        private String ddlsession;
        private String usercode;
        private String TFA;

        String returnstring;

        public BreederDataDownload(int action, String ddlyear, String ddlsession, String usercode, String TFA) {
            this.action = action;
            this.ddlyear = ddlyear;
            this.ddlsession = ddlsession;
            this.usercode = usercode;
            this.TFA = TFA;
        }

        protected void onPreExecute() {
            // dialog.setMessage("Loading....");
            //dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            HttpClient httpclient = new DefaultHttpClient();
            //HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), 10000); // Timeout Limit

            StringBuilder builder = new StringBuilder();
            List<NameValuePair> postParameters = new ArrayList<NameValuePair>(2);
            postParameters.add(new BasicNameValuePair("from", "insertDownloadData"));
            // postParameters.add(new BasicNameValuePair("xmlString",""));
            // String Urlpath1 = cx.Bredderurlpath + "?action=" + action + "&ddlyear=" + ddlyear + "&ddlsession=" + "K,S,R" + "&usercode=" + usercode + "&TFA=" + TFA + "";
            String Urlpath1 = cx.Bredderurlpath + "?action=" + action + "&ddlyear=" + ddlyear + "&ddlsession=" + ddlsession + "&usercode=" + usercode + "&TFA=" + TFA + "";
            HttpPost httppost = new HttpPost(Urlpath1);
            Log.d("rohitt", "doInBackground: " + Urlpath1);
            // StringEntity entity;
            // entity = new StringEntity(request, HTTP.UTF_8);

            httppost.addHeader("Content-type", "application/x-www-form-urlencoded");
            // httppost.setHeader("Content-Type","text/xml;charset=UTF-8");
            try {
                httppost.setEntity(new UrlEncodedFormEntity(postParameters));
                UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
                httppost.setEntity(formEntity);

                HttpResponse response = httpclient.execute(httppost);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }
                    returnstring = builder.toString();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                returnstring = e.getMessage().toString();
                // dialog.dismiss();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                returnstring = e.getMessage().toString();
                //  dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
                returnstring = e.getMessage().toString();
                //  dialog.dismiss();
            }


            // dialog.dismiss();
            return returnstring;
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);


            try {
                new Datadownload(s).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


    public class Datadownload extends AsyncTask<String, String, String> {

        private String s;


        public Datadownload(String s) {
            this.s = s;

        }

        @Override
        protected String doInBackground(String... strings) {
            DownloadData(s);
            return s;
        }


        @Override
        protected void onPostExecute(String s) {


            boolean f1 = DownloadData(s);
            mprogresss.dismiss();
            if (f1) {


                msclass.showMessage("Data Download successfully");


            } else {
                msclass.showMessage("Data Not Downloaded");


            }


        }
    }
}