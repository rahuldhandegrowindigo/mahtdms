package com.newtrail.mahyco.trail;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class login extends AppCompatActivity {
    private TextView txtView, txtentermobile, txtEnterotp, lbltype, txtRegister, txtUpdate, txtForget;
    private EditText otp;
    private CardView btnLogin, btnRegistr, btnUpdate;
    String type = null;

    ProgressDialog dialog;
    public String SERVER = "http://cmr.mahyco.com/BreaderDataHandler.ashx";
    public String langcode = "";
    SharedPreferences pref;
    String imeiNumber;
    SharedPreferences.Editor editor;
    ProgressDialog progressdialog, bar;
    private ProgressDialog mProgressDialog;
    SharedPreferences locdata;
    public Messageclass msclass;
    private Boolean exit = false;
    public CommonExecution cx;
    databaseHelper databaseHelper1;
    Config config;
    public String userCode;
    public String userMail;
    public static final int Progress_Dialog_Progress = 0;
    String currentVersion, latestVersion;
    Dialog dialog1;
    private BroadcastReceiver receiver;
    private long enqueue;
    private DownloadManager dm;
    String imeicheck;
    boolean loginState;

    private static final int RC_SIGN_IN = 1000;
    GoogleSignInClient mGoogleSignInClient;
    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle("Login");
        // Bundle extras = getIntent().getExtras();
        // if (extras!= null)
        //{
        //   type= extras.getString("TYPE");
        //}
        //lbltype=(TextView)findViewById(R.id.lbltype);
        //lbltype.setText(type);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        btnLogin = (CardView) findViewById(R.id.btnLogin);
        btnRegistr = (CardView) findViewById(R.id.btnRegister);
        btnUpdate = (CardView) findViewById(R.id.btnUpdate);
        txtentermobile = (TextView) findViewById(R.id.txtentermobile);
        txtEnterotp = (TextView) findViewById(R.id.txtEnterotp);

        txtRegister = (TextView) findViewById(R.id.txtRegister);
        txtUpdate = (TextView) findViewById(R.id.txtUpdate);
        txtForget = (TextView) findViewById(R.id.txtForget);
        msclass = new Messageclass(this);
        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        cx = new CommonExecution(this);
        config = new Config(this); //Here the context is passing
        databaseHelper1 = new databaseHelper(this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();

        checkAndRequestPermissions();
        getCurrentVersion();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginRequest();
            }
        });
        /* Open Login activity for Register BP,SA,SO*/
        // txtView= (TextView) findViewById(R.id.textView8);
        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenRegister();
            }
        });
        txtForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenForget();
            }
        });
        Cursor data = databaseHelper1.fetchusercode();

        if (data.getCount() == 0) {

        } else {
            data.moveToFirst();
            if (data != null) {
                do {
                    userCode = data.getString((data.getColumnIndex("user_code")));
                } while (data.moveToNext());

            }
            data.close();
            txtentermobile.setText("");

        }

        //For Update App
        txtUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent promptInstall = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=com.newtrial.mahyco.trail"));
                startActivity(promptInstall);
                // downloadcall();
            }
        });


        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        imeiNumber = tm.getDeviceId();


    }

    public void OpenLogin() {
        Intent openIntent = new Intent(this, new_main_menu.class);
        startActivity(openIntent);
    }

    public void OpenRegister() {
        Intent openIntent = new Intent(this, register_user.class);
        startActivity(openIntent);
    }

    public void OpenForget() {
        Intent openIntent = new Intent(this, forget_password.class);
        startActivity(openIntent);
    }

    public void LoginRequest() {


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String username = txtentermobile.getText().toString().trim();
        String pass = txtEnterotp.getText().toString().trim();
        // String lang = spnlanguage.getSelectedItem().toString().trim();
        dialog.setMessage("Loading. Please wait...");
        dialog.show();
        logincheck(username.trim(), pass.trim());
        //  new LoginReq().execute(SERVER,username,pass);

    }

    private void logincheck(String username, String pass) {


//        if (config.NetworkConnection()) {
        try {

            String searchQuery = "select  *  from UserMaster where User_pwd='" + pass.trim() + "' and user_code='" + username.toUpperCase().trim() + "'";
            //  String searchQuery = "SELECT  *  FROM UserMaster  ";
            SQLiteDatabase database = new databaseHelper(this).getReadableDatabase();
            Cursor cursor = database.rawQuery(searchQuery, null);
            //Cursor cursor = mDatabase.getReadableDatabase().rawQuery(searchQuery, null);
            int count = cursor.getCount();

            if (count > 0) {
                // msclass.showMessage(cursor.getString(1));
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    editor.putString("UserID", cursor.getString(1));
                    editor.putString("Pass", cursor.getString(0));
                    editor.putString("Displayname", cursor.getString(3));
                    //editor.putString("IMEI", getDeviceIMEI());
                    editor.commit();
                    cursor.moveToNext();
                }

                cursor.close();
                txtEnterotp.setText("");
                txtentermobile.setText("");
                Intent intent = new Intent(login.this, home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                dialog.dismiss();

            } else {
                msclass.showMessage("User name and password not correct ,please try again");
                dialog.dismiss();
            }
        } catch (Exception ex) {
            msclass.showMessage(ex.getMessage());
        }

//   }

//   else {
//            msclass.showMessage("Internet network not available.");
//            dialog.dismiss();
//
//
//        }

    }

    private void getCurrentVersion() {
        PackageManager pm = this.getPackageManager();
        PackageInfo pInfo = null;

        try {
            pInfo = pm.getPackageInfo(this.getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        currentVersion = pInfo.versionName;

        new GetLatestVersion().execute();
    }

    private class GetLatestVersion extends AsyncTask<String, String, JSONObject> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
//It retrieves the latest version by scraping the content of current version from play store at runtime
                String urlOfAppFromPlayStore = "https://play.google.com/store/apps/details?id=com.newtrail.mahyco.trail";
                Document doc = Jsoup.connect(urlOfAppFromPlayStore).get();
                latestVersion = doc.getElementsByClass("htlgb").get(6).text();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if (latestVersion != null) {
                if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                    if (!isFinishing()) { //This would help to prevent Error : BinderProxy@45d459c0 is not valid; is your activity running? error
                        showUpdateDialog();
                    } else {
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                dowork();
//                                startapp();
//                                finish();
//                            }
//                        }).start();

                    }
                }
            } else
                // background.start();
                super.onPostExecute(jsonObject);
        }
    }

    private void showUpdateDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("A New Update is Available");
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                        ("https://play.google.com/store/apps/details?id=com.newtrail.mahyco.trail")));
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //background.start();
            }
        });

        builder.setCancelable(false);
        dialog1 = builder.show();
    }

    private boolean checkAndRequestPermissions() {
        int permissionCAMERA = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.CAMERA);

        int storagePermission = ContextCompat.checkSelfPermission(this,

                android.Manifest.permission.READ_EXTERNAL_STORAGE);


        int READ_PHONE_STATE = ContextCompat.checkSelfPermission(this,

                android.Manifest.permission.READ_PHONE_STATE);
        int WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(this,

                android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int ACCESS_FINE_LOCATION = ContextCompat.checkSelfPermission(this,

                android.Manifest.permission.ACCESS_FINE_LOCATION);
        int ACCESS_COARSE_LOCATION = ContextCompat.checkSelfPermission(this,

                android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int USE_FINGERPRINT = ContextCompat.checkSelfPermission(this,

                android.Manifest.permission.USE_FINGERPRINT);
        int INTERNET = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.INTERNET);

        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.SEND_SMS);

        int receiveSMS = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.RECEIVE_SMS);

        int readSMS = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_SMS);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (INTERNET != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.INTERNET);
        }
        if (READ_PHONE_STATE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_PHONE_STATE);
        }
        if (WRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (ACCESS_FINE_LOCATION != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (ACCESS_COARSE_LOCATION != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (USE_FINGERPRINT != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.USE_FINGERPRINT);
        }

        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.SEND_SMS);
        }
        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.RECEIVE_SMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_REQUEST_ACCOUNTS);
            return false;
        }
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        Log.d("rht", "onStart: " + account);
        //updateUI(account);
    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            final GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                // Preferences.saveBool(getApplicationContext(), Preferences.KEY_IS_LOGGED_IN, true);


                Map<String, Object> user = new HashMap<>();
                user.put("email", account.getEmail());
                user.put("familyName", account.getFamilyName());
                user.put("fullName", account.getDisplayName());
                user.put("givenName", account.getGivenName());
                //user.put("loginTime", getDate());


                // txtData.setText(account.getDisplayName() + "::::" + account.getId() + "::::" + account.getIdToken() + "::::" + account.getEmail());
                // Toast.makeText(this, "acc::"+account.toString(), Toast.LENGTH_SHORT).show();
            }
            Log.d("rht", "handleSignInResult: " + account.toString());
            Toast.makeText(this, "acc::"+account.toString(), Toast.LENGTH_SHORT).show();


            if (!(account.toString().isEmpty())) {

                Log.d("rht", "mailUser: " + account.getEmail().toString());
                Log.d("rht", "Error: " + account.getServerAuthCode());

              // String email="rahul.dhande@mahyco.com";
               // UserRegisterGoogle(email);
                UserRegisterGoogle(account.getEmail().toString());

            } else {
                msclass.showMessage("Something went Wrong");
           }
            // Signed in successfully, show authenticated UI.
            // updateUI(account);
        } catch (ApiException e) {
            e.getMessage();
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("rht", "signInResult:failed code=" + e.getStatusCode());
            //updateUI(null);
            Log.d("rht", "handleSignInResult: " +    e.getMessage());
        }
    }


    private  boolean UserRegisterGoogle(String userMail)
    {
        if(config.NetworkConnection()) {
            dialog.setMessage("Loading....");
            dialog.show();
            String str= null;
            boolean fl=false;
            try {
                str = cx.new BreederMasterGoogleData(userMail).execute().get();

                if(str.contains("False"))
                {
                    msclass.showMessage("You are not authorised. Kindly register");
                    dialog.dismiss();
                }


                else {
                    JSONObject object = new JSONObject(str);
                    Log.d("","DataWe"+object);
                    JSONArray jArray = object.getJSONArray("Table");
                    Log.d("","DataWe"+jArray);

                    for (int i = 0; i < jArray.length(); i++) {

                        JSONObject jObject = jArray.getJSONObject(0);

                        Log.d("","DataWe"+jObject);
//                        if(jObject.getString("IMEI")!=(imeiNumber)) {
//
//                            msclass.showMessage("Already Registered on other device");//show specific response msg from api
//                            dialog.dismiss();
//
//                        }
                        databaseHelper1.deleledata("UserMaster", "");
                        fl = databaseHelper1.InsertUserRegistrationGoogle(jObject.getString("USER_CODE").toString().toUpperCase(), jObject.getString("USER_NAME").toString(), jObject.getString("USER_PWD").toString(), jObject.getString("USER_ROLE").toString(),jObject.getString("IMEI").toString(),userMail);
                        editor.putString("Displayname", jObject.getString("USER_NAME").toString());

                        editor.putString("USER_IMEINO",jObject.getString("IMEI"));


                        //}
                        //else
                        // {
                        //    msclass.showMessage("This user mobile device IMEI No and User id not match .please check IMEI_no");
                        //    dialog.dismiss();
                        ////    return false;
                        //}


                    }
                    editor.putString("USER_CODE", txtentermobile.getText().toString());
                    editor.putString("USER_EMAIL", userMail.toString());
                    editor.commit();

                    if (fl == true) {
                        //    msclass.showMessage("User Registration successfully");
                        dialog.dismiss();
                        loginState=true;
                        txtEnterotp.setText("");
                        txtentermobile.setText("");
                        editor.putBoolean("loginState",loginState);
                        Intent intent= new Intent(getApplicationContext(),home.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        return true;
                        // finish();

                    } else {
                        msclass.showMessage("Registration  not done");
                        return false;
                    }


                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else
        {
            msclass.showMessage("Internet network not available.");
            dialog.dismiss();
            return false;
        }
        return true;
    }
}