package com.newtrail.mahyco.trail;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;

import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.newtrail.mahyco.trail.ReportDashboard.ReportDashboard;
import com.newtrail.mahyco.trail.TravelManagement.MyTravel;

public class home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    NavigationView navigationView = null;

    public static boolean isBackNavigationAllowed=true;
    String userRole="";
    DrawerLayout drawerLayout;
    databaseHelper databaseHelper1;
    Toolbar toolbar = null;
    SharedPreferences pref;
    boolean loginState;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        databaseHelper1 = new databaseHelper(this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        Cursor data = databaseHelper1.fetchusercode();
        loginState=pref.getBoolean("loginState",false);
        if (data.getCount() == 0) {

        } else {
            data.moveToFirst();
            if (data != null) {
                do {
                    userRole = data.getString((data.getColumnIndex("USER_ROLE")));


                    Log.d("Role", "Role" + userRole);
                } while (data.moveToNext());

            }
            data.close();


        }


        //Set Fragment on initial Rahul Dhande
        //MainMenu mainMenu=new MainMenu();
        //isBackNavigationAllowed=false;
        new_main_menu new_main_menu = new new_main_menu();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.FragmentContainer, new_main_menu);
        fragmentTransaction.commit();
        //end
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Main Menu");

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();


        if (userRole.equals("1")) {

            nav_Menu.findItem(R.id.nav_home).setVisible(true);
            nav_Menu.findItem(R.id.nav_download).setVisible(true);
            nav_Menu.findItem(R.id.nav_DwdObservation).setVisible(true);
            nav_Menu.findItem(R.id.nav_area).setVisible(true);
            nav_Menu.findItem(R.id.nav_observation).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload).setVisible(true);
            nav_Menu.findItem(R.id.nav_report).setVisible(true);
            nav_Menu.findItem(R.id.nav_my_travel).setVisible(true);
            nav_Menu.findItem(R.id.nav_map).setVisible(true);
            nav_Menu.findItem(R.id.nav_exit).setVisible(true);


        } else if (userRole.equals("2")) {


            nav_Menu.findItem(R.id.nav_home).setVisible(true);
            nav_Menu.findItem(R.id.nav_download).setVisible(true);
            nav_Menu.findItem(R.id.nav_DwdObservation).setVisible(true);
            nav_Menu.findItem(R.id.nav_area).setVisible(true);
            nav_Menu.findItem(R.id.nav_observation).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload).setVisible(true);
            nav_Menu.findItem(R.id.nav_report).setVisible(true);
            nav_Menu.findItem(R.id.nav_my_travel).setVisible(true);
            nav_Menu.findItem(R.id.nav_map).setVisible(false);
            nav_Menu.findItem(R.id.nav_exit).setVisible(true);


        } else if (userRole.equals("3")) {


            nav_Menu.findItem(R.id.nav_home).setVisible(true);
            nav_Menu.findItem(R.id.nav_download).setVisible(true);
            nav_Menu.findItem(R.id.nav_DwdObservation).setVisible(true);
            nav_Menu.findItem(R.id.nav_area).setVisible(true);
            nav_Menu.findItem(R.id.nav_observation).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload).setVisible(true);
            nav_Menu.findItem(R.id.nav_report).setVisible(true);
            nav_Menu.findItem(R.id.nav_my_travel).setVisible(true);
            nav_Menu.findItem(R.id.nav_map).setVisible(true);
            nav_Menu.findItem(R.id.nav_exit).setVisible(true);


        } else if (userRole.equals("4")) {


            nav_Menu.findItem(R.id.nav_home).setVisible(true);
            nav_Menu.findItem(R.id.nav_download).setVisible(true);
            nav_Menu.findItem(R.id.nav_DwdObservation).setVisible(false);
            nav_Menu.findItem(R.id.nav_area).setVisible(false);
            nav_Menu.findItem(R.id.nav_observation).setVisible(false);
            nav_Menu.findItem(R.id.nav_upload).setVisible(true);
            nav_Menu.findItem(R.id.nav_report).setVisible(true);
            nav_Menu.findItem(R.id.nav_my_travel).setVisible(false);
            nav_Menu.findItem(R.id.nav_map).setVisible(true);
            nav_Menu.findItem(R.id.nav_exit).setVisible(true);


        } else if (userRole.equals("5")) {


            nav_Menu.findItem(R.id.nav_home).setVisible(true);
            nav_Menu.findItem(R.id.nav_download).setVisible(true);
            nav_Menu.findItem(R.id.nav_DwdObservation).setVisible(false);
            nav_Menu.findItem(R.id.nav_area).setVisible(false);
            nav_Menu.findItem(R.id.nav_observation).setVisible(false);
            nav_Menu.findItem(R.id.nav_upload).setVisible(true);
            nav_Menu.findItem(R.id.nav_report).setVisible(true);
            nav_Menu.findItem(R.id.nav_my_travel).setVisible(false);
            nav_Menu.findItem(R.id.nav_map).setVisible(true);
            nav_Menu.findItem(R.id.nav_exit).setVisible(true);


        } else if (userRole.equals("6")) {


            nav_Menu.findItem(R.id.nav_home).setVisible(true);
            nav_Menu.findItem(R.id.nav_download).setVisible(true);
            nav_Menu.findItem(R.id.nav_DwdObservation).setVisible(true);
            nav_Menu.findItem(R.id.nav_area).setVisible(true);
            nav_Menu.findItem(R.id.nav_observation).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload).setVisible(true);
            nav_Menu.findItem(R.id.nav_report).setVisible(true);
            nav_Menu.findItem(R.id.nav_my_travel).setVisible(true);
            nav_Menu.findItem(R.id.nav_map).setVisible(false);
            nav_Menu.findItem(R.id.nav_exit).setVisible(true);


        } else if (userRole.equals("7")) {


            nav_Menu.findItem(R.id.nav_home).setVisible(true);
            nav_Menu.findItem(R.id.nav_download).setVisible(true);
            nav_Menu.findItem(R.id.nav_DwdObservation).setVisible(false);
            nav_Menu.findItem(R.id.nav_area).setVisible(false);
            nav_Menu.findItem(R.id.nav_observation).setVisible(false);
            nav_Menu.findItem(R.id.nav_upload).setVisible(true);
            nav_Menu.findItem(R.id.nav_report).setVisible(true);
            nav_Menu.findItem(R.id.nav_my_travel).setVisible(false);
            nav_Menu.findItem(R.id.nav_map).setVisible(true);
            nav_Menu.findItem(R.id.nav_exit).setVisible(true);


        }else{



            nav_Menu.findItem(R.id.nav_home).setVisible(true);
            nav_Menu.findItem(R.id.nav_download).setVisible(true);
            nav_Menu.findItem(R.id.nav_DwdObservation).setVisible(true);
            nav_Menu.findItem(R.id.nav_area).setVisible(true);
            nav_Menu.findItem(R.id.nav_observation).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload).setVisible(true);
            nav_Menu.findItem(R.id.nav_report).setVisible(true);
            nav_Menu.findItem(R.id.nav_my_travel).setVisible(true);
            nav_Menu.findItem(R.id.nav_map).setVisible(true);
            nav_Menu.findItem(R.id.nav_exit).setVisible(true);

        }


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (!isBackNavigationAllowed) {
                logOutAlert().show();
            } else {
                super.onBackPressed();
            }
        }

    }
    private AlertDialog logOutAlert() {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Are you sure you want to exit from app?")

                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        loginState=false;
                        editor.putBoolean("loginState",loginState);
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            //Set Fragment on initial Rahul Dhande
            // MainMenu mainMenu = new MainMenu();
            // android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            // fragmentTransaction.replace(R.id.FragmentContainer,mainMenu).addToBackStack("fragBack");
            // fragmentTransaction.commit();

            new_main_menu new_main_menu = new new_main_menu();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.FragmentContainer, new_main_menu).addToBackStack("fragBack");
            fragmentTransaction.commit();
            //end
        } else if (id == R.id.nav_download) {
            //Set Fragment on initial Rahul Dhande
            isBackNavigationAllowed=true;
            DownloadData mainMenu = new DownloadData();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.FragmentContainer, mainMenu).addToBackStack("fragBack");
            fragmentTransaction.commit();

            //end
        } else if (id == R.id.nav_DwdObservation) {
            //Set Fragment on initial Rahul Dhande
            isBackNavigationAllowed=true;
            new_Download_Observation mainMenu = new new_Download_Observation();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.FragmentContainer, mainMenu).addToBackStack("fragBack");
            fragmentTransaction.commit();

            //end
        } else if (id == R.id.nav_area) {
            //Set Fragment on initial Rahul Dhande
            isBackNavigationAllowed=true;
            FieldAreaTag mainMenu = new FieldAreaTag();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.FragmentContainer, mainMenu).addToBackStack("fragBack");
            fragmentTransaction.commit();

            //end
        } else if (id == R.id.nav_observation) {
            //Set Fragment on initial Rahul Dhande
            isBackNavigationAllowed=true;
            Observation mainMenu = new Observation();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.FragmentContainer, mainMenu).addToBackStack("fragBack");
            fragmentTransaction.commit();

            //end
        } else if (id == R.id.nav_validation) {isBackNavigationAllowed=true;
            //Set Fragment on initial Rahul Dhande
            Validation mainMenu = new Validation();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.FragmentContainer, mainMenu).addToBackStack("fragBack");
            fragmentTransaction.commit();

            //end
        } else if (id == R.id.nav_upload) {isBackNavigationAllowed=true;
            //Set Fragment on initial Rahul Dhande
            UploadData mainMenu = new UploadData();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.FragmentContainer, mainMenu).addToBackStack("fragBack");
            fragmentTransaction.commit();
            //end
        } else if (id == R.id.nav_report) {
            //Set Fragment on initial Rahul Dhande
            Intent i = new Intent(this, ReportDashboard.class);
            startActivity(i);
            //end
        } else if (id == R.id.nav_my_travel) {
            //Set Fragment on initial Rahul Dhande
            Intent i = new Intent(this, MyTravel.class);
            startActivity(i);
            //end
        } else if (id == R.id.nav_db) {
            Intent i = new Intent(this, AndroidDatabaseManager.class);
            startActivity(i);
        } else if (id == R.id.nav_map) {
            Intent i = new Intent(this, MapsActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_exit) {
           // editor.putString("UserID", null);
            editor.clear();
            editor.commit();
            //Set Fragment on initial Rahul Dhande
            Logout mainMenu = new Logout();
            Intent openIntent = new Intent(this, login.class);
            openIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(openIntent);
            //end

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
